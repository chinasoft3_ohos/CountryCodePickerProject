package com.hbb20;


import com.hbb20.utils.Log;

/**
 * CCPTalkBackTextProvider
 *
 * @author a
 * @version 1.0
 */
public interface CCPTalkBackTextProvider {
    /**
     * getTalkBackTextForCountry
     *
     * @param country country
     * @return String
     */
    String getTalkBackTextForCountry(CCPCountry country);
}

class InternalTalkBackTextProvider implements CCPTalkBackTextProvider {
    @Override
    public String getTalkBackTextForCountry(CCPCountry country) {
        if (country == null) {
            return null;
        } else {
            return country.name + " phone code is +" + country.phoneCode;
        }
    }
}


