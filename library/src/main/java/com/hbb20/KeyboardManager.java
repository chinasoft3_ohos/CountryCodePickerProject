/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hbb20;

import java.util.ArrayList;
import java.util.List;

/**
 * KeyboardManager.
 *
 * @author author
 * @version version
 */
public class KeyboardManager {
    private static KeyboardManager keyboard;

    private static List<KeyboardListener> list = new ArrayList<>();

    /**
     * get.
     *
     * @return KeyboardManager
     */
    public static KeyboardManager get() {
        if (keyboard == null) {
            keyboard = new KeyboardManager();
        }
        return keyboard;
    }

    /**
     * addKeyKeyboardListener.
     *
     * @param keyboardListener keyboardListener
     */
    public void addKeyKeyboardListener(KeyboardListener keyboardListener) {
        if (list == null) {
            list = new ArrayList<>();
        }
        list.add(keyboardListener);
    }

    /**
     * removeKeyboardListener.
     *
     * @param keyboardListener keyboardListener
     */
    public void removeKeyboardListener(KeyboardListener keyboardListener) {
        if (list == null) {
            return;
        }
        list.remove(keyboardListener);
    }

    /**
     * dispatchListener.
     *
     * @param show   show
     * @param height height
     */
    public void dispatchListener(boolean show, int height) {
        if (list == null || list.isEmpty()) {
            return;
        }
        list.forEach(keyboardListener -> keyboardListener.onKeyboardListener(show, height));
    }

    /**
     * KeyboardListener.
     */
    public interface KeyboardListener {
        /**
         * onKeyboardListener
         *
         * @param show   show
         * @param height height
         */
        void onKeyboardListener(boolean show, int height);
    }

}
