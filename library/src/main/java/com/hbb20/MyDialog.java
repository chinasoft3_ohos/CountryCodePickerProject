/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hbb20;

import ohos.agp.window.dialog.CommonDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.app.Context;

/**
 * MyDialog.
 *
 * @author author
 * @version version
 */
public class MyDialog extends CommonDialog implements KeyboardManager.KeyboardListener {
    private OnDismissListener mOnDismissListener;

    private KeyboardManager.KeyboardListener mKeyboardListener;

    /**
     * setKeyboardListener.
     *
     * @param keyboardListener keyboardListener
     */
    public void setKeyboardListener(KeyboardManager.KeyboardListener keyboardListener) {
        this.mKeyboardListener = keyboardListener;
    }

    /**
     * setOnDismissListener.
     *
     * @param onDismissListener onDismissListener
     */
    public void setOnDismissListener(OnDismissListener onDismissListener) {
        this.mOnDismissListener = onDismissListener;
    }

    /**
     * context.
     *
     * @param context
     */
    public MyDialog(Context context) {
        super(context);
        KeyboardManager.get().addKeyKeyboardListener(this);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        KeyboardManager.get().removeKeyboardListener(this);
        if (mOnDismissListener != null) {
            mOnDismissListener.onDismiss(this);
        }
    }

    @Override
    public void onKeyboardListener(boolean show, int height) {
        if (show) {
            getContentCustomComponent().setPaddingBottom(height - 100);
        } else {
            getContentCustomComponent().setPaddingBottom(0);
        }
        if (mKeyboardListener != null) {
            mKeyboardListener.onKeyboardListener(show, height);
        }
    }

    /**
     * OnDismissListener.
     */
    interface OnDismissListener {
        /**
         * onDismiss.
         *
         * @param dialog
         */
        void onDismiss(IDialog dialog);
    }
}
