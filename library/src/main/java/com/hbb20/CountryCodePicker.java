package com.hbb20;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import com.futuremind.recyclerviewfastscroll.ImageTint;
import io.michaelrocks.libphonenumber.ohos.LogUtils;
import io.michaelrocks.libphonenumber.ohos.NumberParseException;
import io.michaelrocks.libphonenumber.ohos.PhoneNumberUtil;
import io.michaelrocks.libphonenumber.ohos.Phonenumber;
import com.hbb20.utils.*;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.Element;
import ohos.agp.components.element.PixelMapElement;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.render.BlendMode;
import ohos.agp.text.Font;
import ohos.agp.utils.Color;
import ohos.agp.utils.TextTool;
import ohos.agp.window.dialog.BaseDialog;
import ohos.agp.window.dialog.IDialog;
import ohos.app.Context;
import ohos.global.resource.Resource;
import ohos.media.image.ImageSource;
import ohos.media.image.PixelMap;
import ohos.media.image.common.ImageInfo;
import ohos.media.image.common.Size;
import ohos.telephony.RadioInfoManager;
import ohos.telephony.SimInfoManager;
import ohos.telephony.TelephoneNumberUtils;

/**
 * Created by hbb20 on 11/1/16.
 */
public class CountryCodePicker extends DependentLayout implements Component.BindStateChangedListener {
    static final int DEFAULT_UNSET = -99;
    static String TAG = "CCP";
    static String BUNDLE_SELECTED_CODE = "selectedCode";
    static int LIB_DEFAULT_COUNTRY_CODE = 91;
    private static int TEXT_GRAVITY_LEFT = -1, TEXT_GRAVITY_RIGHT = 1, TEXT_GRAVITY_CENTER = 0;
    private CCPTalkBackTextProvider talkBackTextProvider = new InternalTalkBackTextProvider();
    int defaultCountryCode;
    String defaultCountryNameCode;
    Context context;
    Component holderView;
    LayoutScatter mInflater;
    Text textView_selectedCountry;
    TextField editText_registeredCarrierNumber;
    DependentLayout holder;
    Image imageViewArrow;
    Image imageViewFlag;
    DirectionalLayout linearFlagBorder;
    DirectionalLayout linearFlagHolder;
    CCPCountry selectedCCPCountry;
    CCPCountry defaultCCPCountry;
    DependentLayout relativeClickConsumer;
    CountryCodePicker codePicker;
    TextGravity currentTextGravity;
    String originalHint = "";
    int ccpPadding;
    // see attr.xml to see corresponding values for pref
    AutoDetectionPref selectedAutoDetectionPref = AutoDetectionPref.SIM_NETWORK_LOCALE;
    PhoneNumberUtil phoneUtil;
    boolean showNameCode = true;
    boolean showPhoneCode = true;
    boolean ccpDialogShowPhoneCode = true;
    boolean showFlag = true;
    boolean showFullName = false;
    boolean showFastScroller = true;
    boolean ccpDialogShowTitle = true;
    boolean ccpDialogShowFlag = true;
    boolean searchAllowed = true;
    boolean showArrow = true;
    boolean showCloseIcon = false;
    boolean rememberLastSelection = false;
    boolean detectCountryWithAreaCode = true;
    boolean ccpDialogShowNameCode = true;
    boolean ccpDialogInitialScrollToSelection = false;
    boolean ccpUseEmoji = false;
    boolean ccpUseDummyEmojiForPreview = false;
    boolean internationalFormattingOnly = true;
    PhoneNumberType hintExampleNumberType = PhoneNumberType.MOBILE;
    String selectionMemoryTag = "ccp_last_selection";
    int contentColor = DEFAULT_UNSET;
    int arrowColor = DEFAULT_UNSET;
    //  Typeface dialogTypeFace;
    Font dialogTypeFace;
    int dialogTypeFaceStyle;
    List<CCPCountry> preferredCountries;
    int ccpTextgGravity = TEXT_GRAVITY_CENTER;
    //this will be "AU,IN,US"
    String countryPreference;
    int fastScrollerBubbleColor = 0;
    List<CCPCountry> customMasterCountriesList;
    //this will be "AU,IN,US"
    String customMasterCountriesParam, excludedCountriesParam;
    Language customDefaultLanguage = Language.ENGLISH;
    Language languageToApply = Language.ENGLISH;

    int flagSize = 0;

    boolean dialogKeyboardAutoPopup = true;
    boolean ccpClickable = true;
    boolean autoDetectLanguageEnabled = false, autoDetectCountryEnabled = false, numberAutoFormattingEnabled = true, hintExampleNumberEnabled = false;
    //String xmlWidth = "notSet";
    int xmlWidth = LayoutConfig.MATCH_CONTENT;
    Text.TextObserver validityTextWatcher;
    InternationalPhoneTextWatcher formattingTextWatcher;
    boolean reportedValidity;
    //   TextWatcher areaCodeCountryDetectorTextWatcher;
    Text.TextObserver areaCodeCountryDetectorTextWatcher;
    boolean countryDetectionBasedOnAreaAllowed;
    String lastCheckedAreaCode = null;
    //int lastCursorPosition = 0;
    boolean countryChangedDueToAreaCode = false;
    private OnCountryChangeListener onCountryChangeListener;
    private PhoneNumberValidityChangeListener phoneNumberValidityChangeListener;
    private FailureListener failureListener;
    private DialogEventsListener dialogEventsListener;
    private CustomDialogTextProvider customDialogTextProvider;
    private int fastScrollerHandleColor = 0;
    private Element dialogBackground;
    private String backgroundGradientColors;
    private String backgroundGradientOrientation;
    private int dialogBackgroundColor, dialogTextColor, dialogSearchEditTextTintColor;
    private int fastScrollerBubbleTextAppearance = 0;
    private float dialogCornerRadius;
    private CCPCountryGroup currentCountryGroup;
    private ClickedListener customClickListener;
    ClickedListener countryCodeHolderClickListener = new ClickedListener() {
        @Override
        public void onClick(Component component) {
            if (customClickListener == null) {
                if (isCcpClickable()) {
                    if (ccpDialogInitialScrollToSelection) {
                        launchCountrySelectionDialog(getSelectedCountryNameCode());
                    } else {
                        launchCountrySelectionDialog();
                    }
                }
            } else {
                customClickListener.onClick(component);
            }
        }
    };

    public CountryCodePicker(Context context) {
        this(context, null);
    }

    public CountryCodePicker(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public CountryCodePicker(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        this.context = context;

        init(attrSet);
    }

    private boolean isNumberAutoFormattingEnabled() {
        return numberAutoFormattingEnabled;
    }

    /**
     * This will set boolean for numberAutoFormattingEnabled and refresh formattingTextWatcher
     *
     * @param numberAutoFormattingEnabled
     */
    public void setNumberAutoFormattingEnabled(boolean numberAutoFormattingEnabled) {
        this.numberAutoFormattingEnabled = numberAutoFormattingEnabled;
        if (editText_registeredCarrierNumber != null) {
            updateFormattingTextWatcher();
        }
    }

    private boolean isInternationalFormattingOnlyEnabled() {
        return internationalFormattingOnly;
    }

    /**
     * This will set boolean for internationalFormattingOnly and refresh formattingTextWatcher
     *
     * @param internationalFormattingOnly
     */
    public void setInternationalFormattingOnly(boolean internationalFormattingOnly) {
        this.internationalFormattingOnly = internationalFormattingOnly;
        if (editText_registeredCarrierNumber != null) {
            updateFormattingTextWatcher();
        }
    }

    private void init(AttrSet attrs) {

        setBindStateChangedListener(this);


        mInflater = LayoutScatter.getInstance(context);

        if (attrs != null) {
            xmlWidth = AttrUtils.getDimensionFromAttr(attrs, "width", xmlWidth);
        }
        removeAllComponents();

        if (attrs != null && xmlWidth == LayoutConfig.MATCH_PARENT) {
            holderView = mInflater.parse(ResourceTable.Layout_layout_full_width_code_picker, this, true);
        } else {
            holderView = mInflater.parse(ResourceTable.Layout_layout_code_picker, this, true);
        }

        textView_selectedCountry = (Text) holderView.findComponentById(ResourceTable.Id_textView_selectedCountry);
        holder = (DependentLayout) holderView.findComponentById(ResourceTable.Id_countryCodeHolder);
        imageViewArrow = (Image) holderView.findComponentById(ResourceTable.Id_imageView_arrow);
        imageViewFlag = (Image) holderView.findComponentById(ResourceTable.Id_image_flag);
        linearFlagHolder = (DirectionalLayout) holderView.findComponentById(ResourceTable.Id_linear_flag_holder);
        linearFlagBorder = (DirectionalLayout) holderView.findComponentById(ResourceTable.Id_linear_flag_border);
        relativeClickConsumer = (DependentLayout) holderView.findComponentById(ResourceTable.Id_rlClickConsumer);
        codePicker = this;
        if (attrs != null) {
            applyCustomProperty(attrs);
        }
        relativeClickConsumer.setClickedListener(countryCodeHolderClickListener);
    }

    private void applyCustomProperty(AttrSet attrs) {
        //default country code
        try {
            //hide nameCode. If someone wants only phone code to avoid name collision for same country phone code.
            showNameCode = AttrUtils.getBooleanFromAttr(attrs, "ccp_showNameCode", true);

            //number auto formatting
            numberAutoFormattingEnabled = AttrUtils.getBooleanFromAttr(attrs, "ccp_autoFormatNumber", true);

            //show phone code.
            showPhoneCode = AttrUtils.getBooleanFromAttr(attrs, "ccp_showPhoneCode", true);
            //show phone code on dialog
            ccpDialogShowPhoneCode = AttrUtils.getBooleanFromAttr(attrs, "ccpDialog_showPhoneCode", showPhoneCode);
            //show name code on dialog
            ccpDialogShowNameCode = AttrUtils.getBooleanFromAttr(attrs, "ccpDialog_showNameCode", true);
            //show title on dialog
            ccpDialogShowTitle = AttrUtils.getBooleanFromAttr(attrs, "ccpDialog_showTitle", true);
            //show title on dialog
            ccpUseEmoji = AttrUtils.getBooleanFromAttr(attrs, "ccp_useFlagEmoji", false);
            //show title on dialog
            ccpUseDummyEmojiForPreview = AttrUtils.getBooleanFromAttr(attrs, "ccp_useDummyEmojiForPreview", false);
            //show flag on dialog
            ccpDialogShowFlag = AttrUtils.getBooleanFromAttr(attrs, "ccpDialog_showFlag", true);
            //ccpDialog initial scroll to selection
            ccpDialogInitialScrollToSelection = AttrUtils.getBooleanFromAttr(attrs, "ccpDialog_initialScrollToSelection", false);
            //show full name
            showFullName = AttrUtils.getBooleanFromAttr(attrs, "ccp_showFullName", false);

            //show fast scroller
            showFastScroller = AttrUtils.getBooleanFromAttr(attrs, "ccpDialog_showFastScroller", true);
            //bubble color
            fastScrollerBubbleColor = AttrUtils.getColorFromAttr(attrs, "ccpDialog_fastScroller_bubbleColor", 0);
            //scroller handle color
            fastScrollerHandleColor = AttrUtils.getColorFromAttr(attrs, "ccpDialog_fastScroller_handleColor", 0);
            //scroller text appearance
            fastScrollerBubbleTextAppearance = AttrUtils.getIntegerFromAttr(attrs, "ccpDialog_fastScroller_bubbleTextAppearance", 0);

            //auto detect language
            autoDetectLanguageEnabled = AttrUtils.getBooleanFromAttr(attrs, "ccp_autoDetectLanguage", false);
            //detect country from area code
            detectCountryWithAreaCode = AttrUtils.getBooleanFromAttr(attrs, "ccp_areaCodeDetectedCountry", true);

            //remember last selection
            rememberLastSelection = AttrUtils.getBooleanFromAttr(attrs, "ccp_rememberLastSelection", false);

            //example number hint enabled?
            hintExampleNumberEnabled = AttrUtils.getBooleanFromAttr(attrs, "ccp_hintExampleNumber", false);

            //international formatting only
            internationalFormattingOnly = AttrUtils.getBooleanFromAttr(attrs, "ccp_internationalFormattingOnly", true);

            // dialog content padding.
            ccpPadding = AttrUtils.getDimensionFromAttr(attrs, "ccp_padding", 8);


            relativeClickConsumer.setPadding(ccpPadding, ccpPadding, ccpPadding, ccpPadding);

            //example number hint type
            String hintNumberTypeName = AttrUtils.getStringFromAttr(attrs, "ccp_hintExampleNumberType", PhoneNumberType.MOBILE.name());

            hintExampleNumberType = PhoneNumberType.valueOf(hintNumberTypeName);

            //memory tag name for selection
            selectionMemoryTag = AttrUtils.getStringFromAttr(attrs, "ccp_selectionMemoryTag", "CCP_last_selection");
            if (selectionMemoryTag == null) {
                selectionMemoryTag = "CCP_last_selection";
            }

            //country auto detection pref
            String autoDetectionPrefName = AttrUtils.getStringFromAttr(attrs, "ccp_countryAutoDetectionPref", AutoDetectionPref.SIM_NETWORK_LOCALE.name());
            AutoDetectionPref autoDetectionPref = AutoDetectionPref.valueOf(autoDetectionPrefName);
            selectedAutoDetectionPref = AutoDetectionPref.getPrefForValue(String.valueOf(autoDetectionPref.ordinal()));

            //auto detect county
            autoDetectCountryEnabled = AttrUtils.getBooleanFromAttr(attrs, "ccp_autoDetectCountry", false);

            //show arrow
            showArrow = AttrUtils.getBooleanFromAttr(attrs, "ccp_showArrow", true);
            refreshArrowViewVisibility();

            //show close icon
            showCloseIcon = AttrUtils.getBooleanFromAttr(attrs, "ccpDialog_showCloseIcon", false);
            //show flag
            showFlag(AttrUtils.getBooleanFromAttr(attrs, "ccp_showFlag", true));
            int textSize = AttrUtils.getDimensionFromAttr(attrs, "ccp_textSize", 0);
            setFlagSize(textSize);
            //autopop keyboard
            setDialogKeyboardAutoPopup(AttrUtils.getBooleanFromAttr(attrs, "ccpDialog_keyboardAutoPopup", true));

            //if custom default language is specified, then set it as custom else sets english as custom
            String attrLanguage = AttrUtils.getStringFromAttr(attrs, "ccp_defaultLanguage", Language.ENGLISH.name());
            customDefaultLanguage = getLanguageEnum(attrLanguage);
            updateLanguageToApply();

            //custom master list
            customMasterCountriesParam = AttrUtils.getStringFromAttr(attrs, "ccp_customMasterCountries", "");
            excludedCountriesParam = AttrUtils.getStringFromAttr(attrs, "ccp_excludedCountries", "");
            if (!isInEditMode()) {
                refreshCustomMasterList();
            }

            //preference
            countryPreference = AttrUtils.getStringFromAttr(attrs, "ccp_countryPreference", "");
            //as3 is raising problem while rendering preview. to avoid such issue, it will update preferred list only on run time.
            if (!isInEditMode()) {
                refreshPreferredCountries();
            }

            //text gravity
//
            String ccp_textGravity = AttrUtils.getStringFromAttr(attrs, "ccp_textGravity", TextGravity.CENTER.name());
            ccpTextgGravity = TextGravity.valueOf(ccp_textGravity).enumIndex;
            applyTextGravity(ccpTextgGravity);

            //default country
            //AS 3 has some problem with reading list so this is to make CCP preview work
            defaultCountryNameCode = AttrUtils.getStringFromAttr(attrs, "ccp_defaultNameCode", "");
            boolean setUsingNameCode = false;
            if (defaultCountryNameCode != null && defaultCountryNameCode.length() != 0) {
                if (!isInEditMode()) {
                    if (CCPCountry.getCountryForNameCodeFromLibraryMasterList(getContext(), getLanguageToApply(), defaultCountryNameCode) != null) {
                        setUsingNameCode = true;
                        setDefaultCountry(CCPCountry.getCountryForNameCodeFromLibraryMasterList(getContext(), getLanguageToApply(), defaultCountryNameCode));
                        setSelectedCountry(defaultCCPCountry);
                    }
                } else {
                    if (CCPCountry.getCountryForNameCodeFromEnglishList(defaultCountryNameCode) != null) {
                        setUsingNameCode = true;
                        setDefaultCountry(CCPCountry.getCountryForNameCodeFromEnglishList(defaultCountryNameCode));
                        setSelectedCountry(defaultCCPCountry);
                    }
                }

                //when it was not set means something was wrong with name code
                if (!setUsingNameCode) {
                    setDefaultCountry(CCPCountry.getCountryForNameCodeFromEnglishList("IN"));
                    setSelectedCountry(defaultCCPCountry);
                    setUsingNameCode = true;
                }
            }

            //if default country is not set using name code.
            int defaultCountryCode = AttrUtils.getIntegerFromAttr(attrs, "ccp_defaultPhoneCode", -1);
            if (!setUsingNameCode && defaultCountryCode != -1) {
                if (!isInEditMode()) {
                    //if invalid country is set using xml, it will be replaced with LIB_DEFAULT_COUNTRY_CODE
                    if (defaultCountryCode != -1 && CCPCountry.getCountryForCode(getContext(), getLanguageToApply(), preferredCountries, defaultCountryCode) == null) {
                        defaultCountryCode = LIB_DEFAULT_COUNTRY_CODE;
                    }
                    setDefaultCountryUsingPhoneCode(defaultCountryCode);
                    setSelectedCountry(defaultCCPCountry);
                } else {
                    //when it is in edit mode, we will check in english list only.
                    CCPCountry defaultCountry = CCPCountry.getCountryForCodeFromEnglishList(defaultCountryCode + "");
                    if (defaultCountry == null) {
                        defaultCountry = CCPCountry.getCountryForCodeFromEnglishList(LIB_DEFAULT_COUNTRY_CODE + "");
                    }
                    setDefaultCountry(defaultCountry);
                    setSelectedCountry(defaultCountry);
                }
            }

            //if default country is not set using nameCode or phone code, let's set library default as default
            if (getDefaultCountry() == null) {
                setDefaultCountry(CCPCountry.getCountryForNameCodeFromEnglishList("IN"));
                if (getSelectedCountry() == null) {
                    setSelectedCountry(defaultCCPCountry);
                }
            }


            //set auto detected country
            if (isAutoDetectCountryEnabled() && !isInEditMode()) {
                setAutoDetectedCountry(true);
            }

            //set last selection
            if (rememberLastSelection && !isInEditMode()) {
                loadLastSelectedCountryInCCP();
            }

            int arrowColor = AttrUtils.getColorFromAttr(attrs, "ccp_arrowColor", DEFAULT_UNSET);

            setArrowColor(arrowColor);

            //content color
            int contentColor;

            if (isInEditMode()) {
                contentColor = AttrUtils.getColorFromAttr(attrs, "ccp_contentColor", DEFAULT_UNSET);
            } else {
                contentColor = AttrUtils.getColorFromAttr(attrs, "ccp_contentColor", Color.BLACK.getValue());
            }
            if (contentColor != DEFAULT_UNSET) {
                setContentColor(contentColor);
            }

            // flag border color
            int borderFlagColor = 0;
            if (isInEditMode()) {
                borderFlagColor = AttrUtils.getColorFromAttr(attrs, "ccp_flagBorderColor", 0);
            } else {
                borderFlagColor = AttrUtils.getColorFromAttr(attrs, "ccp_flagBorderColor", Color.TRANSPARENT.getValue());
            }

            if (borderFlagColor != 0) {
                setFlagBorderColor(borderFlagColor);
            }

            //dialog colors
            setDialogBackgroundColor(AttrUtils.getColorFromAttr(attrs, "ccpDialog_backgroundColor", 0));
            setDialogBackground(AttrUtils.getElementFromAttr(attrs, "ccpDialog_background"));

            setBackgroundGradientColors(AttrUtils.getStringFromAttr(attrs, "ccpDialog_backgroundGradientColors", ""));
            setBackgroundGradientOrientation(AttrUtils.getStringFromAttr(attrs, "ccpDialog_backgroundGradientOrientation", "TOP_TO_BOTTOM"));

            setDialogTextColor(AttrUtils.getColorFromAttr(attrs, "ccpDialog_textColor", 0));
            setDialogSearchEditTextTintColor(AttrUtils.getColorFromAttr(attrs, "ccpDialog_searchEditTextTint", 0));
            setDialogCornerRaius(AttrUtils.getDimensionFromAttr(attrs, "ccpDialog_cornerRadius", 0));
            //text size

            if (textSize > 0) {
                textView_selectedCountry.setTextSize(textSize, Text.TextSizeType.PX);
                setFlagSize(textSize);
                setArrowSize(textSize);
            }

            //if arrow size is explicitly defined
            int arrowSize = AttrUtils.getDimensionFromAttr(attrs, "ccp_arrowSize", 0);
            if (arrowSize > 0) {
                setArrowSize(arrowSize);
            }

            searchAllowed = AttrUtils.getBooleanFromAttr(attrs, "ccpDialog_allowSearch", true);
            setCcpClickable(AttrUtils.getBooleanFromAttr(attrs, "ccp_clickable", true));
        } catch (Exception e) {
            LogUtils.log("error" + e.getMessage());
        } finally {
        }
    }

    private void refreshArrowViewVisibility() {
        if (showArrow) {
            imageViewArrow.setVisibility(VISIBLE);
        } else {
            imageViewArrow.setVisibility(HIDE);
        }
    }

    /**
     * this will read last selected country name code from the shared pref.
     * if that name code is not null, load that country in the CCP
     * else leaves as it is.(when used for the first time)
     */
    private void loadLastSelectedCountryInCCP() {
        String lastSelectedCountryNameCode = Preferences.get(getContext()).getString(selectionMemoryTag, "");
        //if last selection value is not null, load it into the CCP
        if (!TextTool.isNullOrEmpty(lastSelectedCountryNameCode)) {
            setCountryForNameCode(lastSelectedCountryNameCode);
        }
    }

    /**
     * This will store the selected name code in the preferences
     *
     * @param selectedCountryNameCode name code of the selected country
     */
    void storeSelectedCountryNameCode(String selectedCountryNameCode) {

        Preferences.get(getContext()).putString(selectionMemoryTag, selectedCountryNameCode);
    }

    boolean isCcpDialogShowPhoneCode() {
        return ccpDialogShowPhoneCode;
    }

    /**
     * To show/hide phone code from country selection dialog
     *
     * @param ccpDialogShowPhoneCode
     */
    public void setCcpDialogShowPhoneCode(boolean ccpDialogShowPhoneCode) {
        this.ccpDialogShowPhoneCode = ccpDialogShowPhoneCode;
    }

    /**
     * To show/hide name code from country selection dialog
     *
     * @return boolean
     */
    public boolean getCcpDialogShowNameCode() {
        return this.ccpDialogShowNameCode;
    }

    /**
     * To show/hide name code from country selection dialog
     *
     * @param ccpDialogShowNameCode
     */
    public void setCcpDialogShowNameCode(boolean ccpDialogShowNameCode) {
        this.ccpDialogShowNameCode = ccpDialogShowNameCode;
    }

    /**
     * To show/hide name code from country selection dialog
     *
     * @return boolean
     */
    public boolean getCcpDialogShowTitle() {
        return this.ccpDialogShowTitle;
    }

    /**
     * To show/hide title from country selection dialog
     *
     * @param ccpDialogShowTitle ccpDialogShowTitle
     */
    public void setCcpDialogShowTitle(boolean ccpDialogShowTitle) {
        this.ccpDialogShowTitle = ccpDialogShowTitle;
    }

    /**
     * To show/hide flag from country selection dialog
     *
     * @return boolean
     */
    public boolean getCcpDialogShowFlag() {
        return this.ccpDialogShowFlag;
    }

    /**
     * To show/hide flag from country selection dialog
     *
     * @param ccpDialogShowFlag
     */
    public void setCcpDialogShowFlag(boolean ccpDialogShowFlag) {
        this.ccpDialogShowFlag = ccpDialogShowFlag;
    }

    boolean isShowPhoneCode() {
        return showPhoneCode;
    }

    /**
     * To show/hide phone code from ccp view
     *
     * @param showPhoneCode
     */
    public void setShowPhoneCode(boolean showPhoneCode) {
        this.showPhoneCode = showPhoneCode;
        setSelectedCountry(selectedCCPCountry);
    }

    /**
     * getDialogEventsListener
     *
     * @return registered dialog event listener
     */
    protected DialogEventsListener getDialogEventsListener() {
        return dialogEventsListener;
    }

    /**
     * Dialog events listener will give call backs on various dialog events
     *
     * @param dialogEventsListener
     */
    public void setDialogEventsListener(DialogEventsListener dialogEventsListener) {
        this.dialogEventsListener = dialogEventsListener;
    }

    int getFastScrollerBubbleTextAppearance() {
        return fastScrollerBubbleTextAppearance;
    }

    /**
     * This sets text appearance for fast scroller index character
     *
     * @param fastScrollerBubbleTextAppearance should be reference id of textappereance style. i.e. R.style.myBubbleTextAppearance
     */
    public void setFastScrollerBubbleTextAppearance(int fastScrollerBubbleTextAppearance) {
        this.fastScrollerBubbleTextAppearance = fastScrollerBubbleTextAppearance;
    }

    int getFastScrollerHandleColor() {
        return fastScrollerHandleColor;
    }

    /**
     * This should be the color for fast scroller handle.
     *
     * @param fastScrollerHandleColor
     */
    public void setFastScrollerHandleColor(int fastScrollerHandleColor) {
        this.fastScrollerHandleColor = fastScrollerHandleColor;
    }

    int getFastScrollerBubbleColor() {
        return fastScrollerBubbleColor;
    }

    /**
     * Sets bubble color for fast scroller
     *
     * @param fastScrollerBubbleColor
     */
    public void setFastScrollerBubbleColor(int fastScrollerBubbleColor) {
        this.fastScrollerBubbleColor = fastScrollerBubbleColor;
    }

    TextGravity getCurrentTextGravity() {
        return currentTextGravity;
    }

    /**
     * When width is set "match_parent", this gravity will set placement of text (Between flag and down arrow).
     *
     * @param textGravity expected placement
     */
    public void setCurrentTextGravity(TextGravity textGravity) {
        this.currentTextGravity = textGravity;
        applyTextGravity(textGravity.enumIndex);
    }

    private void applyTextGravity(int enumIndex) {
        if (enumIndex == TextGravity.LEFT.enumIndex) {
            textView_selectedCountry.setTextAlignment(4);
        } else if (enumIndex == TextGravity.CENTER.enumIndex) {
            textView_selectedCountry.setTextAlignment(72);
        } else {
            textView_selectedCountry.setTextAlignment(16);
        }
    }

    /**
     * which language to show is decided based on
     * autoDetectLanguage flag
     * if autoDetectLanguage is true, then it should check language based on locale, if no language is found based on locale, customDefault language will returned
     * else autoDetectLanguage is false, then customDefaultLanguage will be returned.
     *
     * @return
     */
    private void updateLanguageToApply() {
        //when in edit mode, it will return default language only
        if (isInEditMode()) {
            if (customDefaultLanguage != null) {
                languageToApply = customDefaultLanguage;
            } else {
                languageToApply = Language.ENGLISH;
            }
        } else {
            if (isAutoDetectLanguageEnabled()) {
                Language localeBasedLanguage = getCCPLanguageFromLocale();
                if (localeBasedLanguage == null) { //if no language is found from locale
                    if (getCustomDefaultLanguage() != null) { //and custom language is defined
                        languageToApply = getCustomDefaultLanguage();
                    } else {
                        languageToApply = Language.ENGLISH;
                    }
                } else {
                    languageToApply = localeBasedLanguage;
                }
            } else {
                if (getCustomDefaultLanguage() != null) {
                    languageToApply = customDefaultLanguage;
                } else {
                    languageToApply = Language.ENGLISH;  //library default
                }
            }
        }
    }

    private Language getCCPLanguageFromLocale() {
        Locale currentLocale = context.getResourceManager().getConfiguration().getFirstLocale();
        for (Language language : Language.values()) {
            if (language.getCode().equalsIgnoreCase(currentLocale.getLanguage())) {

                if (language.getCountry() == null
                        || language.getCountry().equalsIgnoreCase(currentLocale.getCountry()))
                    return language;

                if (language.getScript() == null
                        || language.getScript().equalsIgnoreCase(currentLocale.getScript()))
                    return language;
            }
        }
        return null;
    }

    private CCPCountry getDefaultCountry() {
        return defaultCCPCountry;
    }

    private void setDefaultCountry(CCPCountry defaultCCPCountry) {
        this.defaultCCPCountry = defaultCCPCountry;
    }

    public Text getTextView_selectedCountry() {
        return textView_selectedCountry;
    }

    public void setTextView_selectedCountry(Text textView_selectedCountry) {
        this.textView_selectedCountry = textView_selectedCountry;
    }

    public Image getImageViewFlag() {
        return imageViewFlag;
    }

    public void setImageViewFlag(Image imageViewFlag) {
        this.imageViewFlag = imageViewFlag;
    }

    private CCPCountry getSelectedCountry() {
        if (selectedCCPCountry == null) {
            setSelectedCountry(getDefaultCountry());
        }
        return selectedCCPCountry;
    }

    void setSelectedCountry(CCPCountry selectedCCPCountry) {
        if (talkBackTextProvider != null && talkBackTextProvider.getTalkBackTextForCountry(selectedCCPCountry) != null) {
            textView_selectedCountry.setComponentDescription(talkBackTextProvider.getTalkBackTextForCountry(selectedCCPCountry));
        }

        //force disable area code country detection
        countryDetectionBasedOnAreaAllowed = false;
        lastCheckedAreaCode = "";

        //as soon as country is selected, textView should be updated
        if (selectedCCPCountry == null) {
            selectedCCPCountry = CCPCountry.getCountryForCode(getContext(), getLanguageToApply(), preferredCountries, defaultCountryCode);
            if (selectedCCPCountry == null) {
                return;
            }
        }

        this.selectedCCPCountry = selectedCCPCountry;

        String displayText = "";

        // add flag if required
        if (showFlag && ccpUseEmoji) {
            if (isInEditMode()) {
                if (ccpUseDummyEmojiForPreview) {
                    //show chequered flag if dummy preview is expected.
                    displayText += "\uD83C\uDFC1\u200B ";
                } else {
                    displayText += CCPCountry.getFlagEmoji(selectedCCPCountry) + "\u200B ";
                }

            } else {
                displayText += CCPCountry.getFlagEmoji(selectedCCPCountry) + "  ";
            }
        }

        // add full name to if required
        if (showFullName) {
            displayText = displayText + selectedCCPCountry.getName();
        }

        // adds name code if required
        if (showNameCode) {
            if (showFullName) {
                displayText += " (" + selectedCCPCountry.getNameCode().toUpperCase() + ")";
            } else {
                displayText += " " + selectedCCPCountry.getNameCode().toUpperCase();
            }
        }

        // hide phone code if required
        if (showPhoneCode) {
            if (displayText.length() > 0) {
                displayText += "  ";
            }
            displayText += "+" + selectedCCPCountry.getPhoneCode();
        }

        textView_selectedCountry.setText(displayText);

        //avoid blank state of ccp
        if (showFlag == false && displayText.length() == 0) {
            displayText += "+" + selectedCCPCountry.getPhoneCode();
            textView_selectedCountry.setText(displayText);
        }

        try {

            Resource resource = getResourceManager().getResource(selectedCCPCountry.getFlagID());
            ImageSource imageSource = ImageSource.create(resource, null);
            ImageInfo imageInfo = imageSource.getImageInfo();
            Size size = imageInfo.size;
            ImageSource.DecodingOptions opts = new ImageSource.DecodingOptions();
            PixelMap pixelmap = imageSource.createPixelmap(opts);
            PixelMapElement pixelMapElement = new PixelMapElement(pixelmap);
            int boundsRight = size.width * flagSize / size.height;
            pixelMapElement.setBounds(0, 0, boundsRight, flagSize);
            imageViewFlag.setImageElement(pixelMapElement);
            ComponentContainer.LayoutConfig layoutConfig = imageViewFlag.getLayoutConfig();
            layoutConfig.width = boundsRight;
            layoutConfig.height = flagSize;
            imageViewFlag.setLayoutConfig(layoutConfig);

        } catch (Exception e) {
            imageViewFlag.setPixelMap(selectedCCPCountry.getFlagID());
        }
        if (onCountryChangeListener != null) {
            onCountryChangeListener.onCountrySelected();
        }
        updateFormattingTextWatcher();

        updateHint();

        //notify to registered validity listener
        if (editText_registeredCarrierNumber != null && phoneNumberValidityChangeListener != null) {
            reportedValidity = isValidFullNumber();
            phoneNumberValidityChangeListener.onValidityChanged(reportedValidity);
        }

        //once updates are done, this will release lock
        countryDetectionBasedOnAreaAllowed = true;

        //if the country was auto detected based on area code, this will correct the cursor position.
        if (countryChangedDueToAreaCode) {
            try {
                countryChangedDueToAreaCode = false;
            } catch (Exception e) {
                Log.i("hh", e.getMessage());
            }
        }

        //update country group
        updateCountryGroup();
    }

    /**
     * update country group
     */
    private void updateCountryGroup() {
        currentCountryGroup = CCPCountryGroup.getCountryGroupForPhoneCode(getSelectedCountryCodeAsInt());
    }

    /**
     * updates hint
     */
    private void updateHint() {
        if (editText_registeredCarrierNumber != null && hintExampleNumberEnabled) {
            String formattedNumber = "";
            Phonenumber.PhoneNumber exampleNumber = getPhoneUtil().getExampleNumberForType(getSelectedCountryNameCode(), getSelectedHintNumberType());
            if (exampleNumber != null) {
                formattedNumber = exampleNumber.getNationalNumber() + "";
                formattedNumber = TelephoneNumberUtils.formatPhoneNumber(getSelectedCountryCodeWithPlus() + formattedNumber, getSelectedCountryNameCode());
                if (formattedNumber != null) {
                    formattedNumber = formattedNumber.substring(getSelectedCountryCodeWithPlus().length()).trim();
                }
            }

            //fallback to original hint
            if (formattedNumber == null) {
                formattedNumber = originalHint;
            }

            editText_registeredCarrierNumber.setHint(formattedNumber);
        }
    }

    /**
     * this function maps CountryCodePicker.PhoneNumberType to PhoneNumberUtil.PhoneNumberType.
     *
     * @return respective PhoneNumberUtil.PhoneNumberType based on selected CountryCodePicker.PhoneNumberType.
     */
    private PhoneNumberUtil.PhoneNumberType getSelectedHintNumberType() {
        switch (hintExampleNumberType) {
            case MOBILE:
                return PhoneNumberUtil.PhoneNumberType.MOBILE;
            case FIXED_LINE:
                return PhoneNumberUtil.PhoneNumberType.FIXED_LINE;
            case FIXED_LINE_OR_MOBILE:
                return PhoneNumberUtil.PhoneNumberType.FIXED_LINE_OR_MOBILE;
            case TOLL_FREE:
                return PhoneNumberUtil.PhoneNumberType.TOLL_FREE;
            case PREMIUM_RATE:
                return PhoneNumberUtil.PhoneNumberType.PREMIUM_RATE;
            case SHARED_COST:
                return PhoneNumberUtil.PhoneNumberType.SHARED_COST;
            case VOIP:
                return PhoneNumberUtil.PhoneNumberType.VOIP;
            case PERSONAL_NUMBER:
                return PhoneNumberUtil.PhoneNumberType.PERSONAL_NUMBER;
            case PAGER:
                return PhoneNumberUtil.PhoneNumberType.PAGER;
            case UAN:
                return PhoneNumberUtil.PhoneNumberType.UAN;
            case VOICEMAIL:
                return PhoneNumberUtil.PhoneNumberType.VOICEMAIL;
            case UNKNOWN:

                return PhoneNumberUtil.PhoneNumberType.UNKNOWN;
            default:
                return PhoneNumberUtil.PhoneNumberType.MOBILE;
        }
    }

    public Language getLanguageToApply() {
        if (languageToApply == null) {
            updateLanguageToApply();
        }
        return languageToApply;
    }

    void setLanguageToApply(Language languageToApply) {
        this.languageToApply = languageToApply;
    }

    private void updateFormattingTextWatcher() {
        if (editText_registeredCarrierNumber != null && selectedCCPCountry != null) {
            String enteredValue = getEditText_registeredCarrierNumber().getText().toString();
            String digitsValue = PhoneNumberUtil.normalizeDigitsOnly(enteredValue);

            if (formattingTextWatcher != null) {
                editText_registeredCarrierNumber.removeTextObserver(formattingTextWatcher);
            }

            if (areaCodeCountryDetectorTextWatcher != null) {
                editText_registeredCarrierNumber.removeTextObserver(areaCodeCountryDetectorTextWatcher);
            }

            if (numberAutoFormattingEnabled) {
                formattingTextWatcher = new InternationalPhoneTextWatcher(context, getSelectedCountryNameCode(), getSelectedCountryCodeAsInt(), internationalFormattingOnly);
                formattingTextWatcher.setTarget(editText_registeredCarrierNumber);
                editText_registeredCarrierNumber.addTextObserver(formattingTextWatcher);
            }

            //if country detection from area code is enabled, then it will add areaCodeCountryDetectorTextWatcher
            if (detectCountryWithAreaCode) {
                areaCodeCountryDetectorTextWatcher = getCountryDetectorTextWatcher(editText_registeredCarrierNumber);
                editText_registeredCarrierNumber.addTextObserver(areaCodeCountryDetectorTextWatcher);
            }

            //text watcher stops working when it finds non digit character in previous phone code. This will reset its function
            editText_registeredCarrierNumber.setText("");
            editText_registeredCarrierNumber.setText(digitsValue);
            //  editText_registeredCarrierNumber.setSelection(editText_registeredCarrierNumber.getText().length());
        } else {
            if (editText_registeredCarrierNumber == null) {
                Log.i(TAG, "updateFormattingTextWatcher: EditText not registered " + selectionMemoryTag);
            } else {
                Log.i(TAG, "updateFormattingTextWatcher: selected country is null " + selectionMemoryTag);
            }
        }
    }

    /**
     * This updates country dynamically as user types in area code
     *
     * @param textField
     * @return Text.TextObserver
     */
    private Text.TextObserver getCountryDetectorTextWatcher(TextField textField) {

        if (editText_registeredCarrierNumber != null) {
            if (areaCodeCountryDetectorTextWatcher == null) {
                areaCodeCountryDetectorTextWatcher = new Text.TextObserver() {
                    String lastCheckedNumber = null;

                    @Override
                    public void onTextUpdated(String s, int i, int i1, int i2) {
                        CCPCountry selectedCountry = getSelectedCountry();
                        if (selectedCountry != null && (lastCheckedNumber == null || !lastCheckedNumber.equals(s.toString())) && countryDetectionBasedOnAreaAllowed) {
                            //possible countries
                            if (currentCountryGroup != null) {
                                String enteredValue = getEditText_registeredCarrierNumber().getText().toString();
                                if (enteredValue.length() >= currentCountryGroup.areaCodeLength) {
                                    String digitsValue = PhoneNumberUtil.normalizeDigitsOnly(enteredValue);
                                    if (digitsValue.length() >= currentCountryGroup.areaCodeLength) {
                                        String currentAreaCode = digitsValue.substring(0, currentCountryGroup.areaCodeLength);
                                        if (!currentAreaCode.equals(lastCheckedAreaCode)) {
                                            CCPCountry detectedCountry = currentCountryGroup.getCountryForAreaCode(context, getLanguageToApply(), currentAreaCode);
                                            if (!detectedCountry.equals(selectedCountry)) {
                                                countryChangedDueToAreaCode = true;
                                                //lastCursorPosition = Selection.getSelectionEnd(s);
                                                setSelectedCountry(detectedCountry);
                                            }
                                            lastCheckedAreaCode = currentAreaCode;
                                        }
                                    }
                                }
                            }
                            lastCheckedNumber = s;
                        }
                    }
                };
            }
        }
        return areaCodeCountryDetectorTextWatcher;
    }

    Language getCustomDefaultLanguage() {
        return customDefaultLanguage;
    }

    private void setCustomDefaultLanguage(Language customDefaultLanguage) {
        this.customDefaultLanguage = customDefaultLanguage;
        updateLanguageToApply();
        setSelectedCountry(CCPCountry.getCountryForNameCodeFromLibraryMasterList(context, getLanguageToApply(), selectedCCPCountry.getNameCode()));
    }

    private Component getHolderView() {
        return holderView;
    }

    private void setHolderView(Component holderView) {
        this.holderView = holderView;
    }

    public DependentLayout getHolder() {
        return holder;
    }

    private void setHolder(DependentLayout holder) {
        this.holder = holder;
    }

    boolean isAutoDetectLanguageEnabled() {
        return autoDetectLanguageEnabled;
    }

    boolean isAutoDetectCountryEnabled() {
        return autoDetectCountryEnabled;
    }

    boolean isDialogKeyboardAutoPopup() {
        return dialogKeyboardAutoPopup;
    }

    /**
     * By default, keyboard pops up every time ccp is clicked and selection dialog is opened.
     *
     * @param dialogKeyboardAutoPopup true: to open keyboard automatically when selection dialog is opened
     *                                false: to avoid auto pop of keyboard
     */
    public void setDialogKeyboardAutoPopup(boolean dialogKeyboardAutoPopup) {
        this.dialogKeyboardAutoPopup = dialogKeyboardAutoPopup;
    }

    boolean isShowFastScroller() {
        return showFastScroller;
    }

    /**
     * Set visibility of fast scroller.
     *
     * @param showFastScroller
     */
    public void setShowFastScroller(boolean showFastScroller) {
        this.showFastScroller = showFastScroller;
    }

    protected boolean isShowCloseIcon() {
        return showCloseIcon;
    }

    /**
     * if true, this will give explicit close icon in CCP dialog
     *
     * @param showCloseIcon
     */
    public void showCloseIcon(boolean showCloseIcon) {
        this.showCloseIcon = showCloseIcon;
    }

    TextField getEditText_registeredCarrierNumber() {
//        Log.d(TAG, "getEditText_registeredCarrierNumber");
        return editText_registeredCarrierNumber;
    }

    /**
     * this will register editText and will apply required text watchers
     *
     * @param editText_registeredCarrierNumber
     */
    void setEditText_registeredCarrierNumber(TextField editText_registeredCarrierNumber) {
        this.editText_registeredCarrierNumber = editText_registeredCarrierNumber;
        if (this.editText_registeredCarrierNumber.getHint() != null) {
            originalHint = this.editText_registeredCarrierNumber.getHint().toString();
        }
        updateValidityTextWatcher();
        updateFormattingTextWatcher();
        updateHint();
    }

    /**
     * This function will
     * - remove existing, if any, validityTextWatcher
     * - prepare new validityTextWatcher
     * - attach validityTextWatcher
     * - do initial reporting to watcher
     */
    private void updateValidityTextWatcher() {
        try {
            editText_registeredCarrierNumber.removeTextObserver(validityTextWatcher);
        } catch (Exception e) {
            LogUtils.log("error" + e.getMessage());
        }

        //initial REPORTING
        reportedValidity = isValidFullNumber();
        if (phoneNumberValidityChangeListener != null) {
            phoneNumberValidityChangeListener.onValidityChanged(reportedValidity);
        }

        validityTextWatcher = new Text.TextObserver() {
            @Override
            public void onTextUpdated(String s, int i, int i1, int i2) {
                if (phoneNumberValidityChangeListener != null) {
                    boolean currentValidity;
                    currentValidity = isValidFullNumber();
                    if (currentValidity != reportedValidity) {
                        reportedValidity = currentValidity;
                        phoneNumberValidityChangeListener.onValidityChanged(reportedValidity);
                    }
                }
            }
        };

        editText_registeredCarrierNumber.addTextObserver(validityTextWatcher);
    }

    private LayoutScatter getmInflater() {
        return mInflater;
    }

    private ClickedListener getCountryCodeHolderClickListener() {
        return countryCodeHolderClickListener;
    }

    int getDialogBackgroundColor() {
        return dialogBackgroundColor;
    }

    /**
     * This will be color of dialog background
     *
     * @param dialogBackgroundColor
     */
    public void setDialogBackgroundColor(int dialogBackgroundColor) {
        this.dialogBackgroundColor = dialogBackgroundColor;
    }

    Element getDialogBackground() {
        return dialogBackground;
    }

    /**
     * This will be color of dialog background
     *
     * @param dialogBackground
     */
    public void setDialogBackground(Element dialogBackground) {
        this.dialogBackground = dialogBackground;
    }

    //颜色渐变，用java 代码实现
    RgbColor[] getBackgroundGradientColors() {
        if (TextTool.isNullOrEmpty(backgroundGradientColors)) {
            return null;
        }
        String[] split = backgroundGradientColors.split(",");
        if (split == null || split.length <= 1) {
            return null;
        }
        RgbColor[] colors = new RgbColor[split.length];
        for (int i = 0; i < split.length; i++) {
            int intColor = Color.getIntColor(split[i]);
            colors[i] = RgbColor.fromArgbInt(intColor);
        }
        return colors;
    }

    public void setBackgroundGradientColors(String colors) {
        this.backgroundGradientColors = colors;
    }

    String getBackgroundGradientOrientation() {
        return this.backgroundGradientOrientation;
    }

    public void setBackgroundGradientOrientation(String orientation) {
        this.backgroundGradientOrientation = orientation;
    }

    int getDialogSearchEditTextTintColor() {
        return dialogSearchEditTextTintColor;
    }

    /**
     * If device is running above or equal LOLLIPOP version, this will change tint of search edittext background.
     *
     * @param dialogSearchEditTextTintColor
     */
    public void setDialogSearchEditTextTintColor(int dialogSearchEditTextTintColor) {
        this.dialogSearchEditTextTintColor = dialogSearchEditTextTintColor;
    }

    public float getDialogCornerRadius() {
        return dialogCornerRadius;
    }

    public void setDialogCornerRaius(float dialogCornerRadius) {
        this.dialogCornerRadius = dialogCornerRadius;
    }

    int getDialogTextColor() {
        return dialogTextColor;
    }

    /**
     * This color will be applied to
     * Title of dialog
     * Name of country
     * Phone code of country
     * "X" button to clear query
     * preferred country divider if preferred countries defined (semi transparent)
     *
     * @param dialogTextColor
     */
    public void setDialogTextColor(int dialogTextColor) {
        this.dialogTextColor = dialogTextColor;
    }

    int getDialogTypeFaceStyle() {
        return dialogTypeFaceStyle;
    }

    /**
     * Publicly available functions from library
     *
     * @return font
     */

    Font getDialogTypeFace() {
        return dialogTypeFace;
    }

    /**
     * To change font of ccp views
     *
     * @param typeFace
     */
    public void setDialogTypeFace(Font typeFace) {
        try {
            dialogTypeFace = typeFace;
            dialogTypeFaceStyle = DEFAULT_UNSET;
        } catch (Exception e) {
            LogUtils.log("error" + e.getMessage());
        }
    }

    /**
     * this will load preferredCountries based on countryPreference
     */
    void refreshPreferredCountries() {
        if (countryPreference == null || countryPreference.length() == 0) {
            preferredCountries = null;
        } else {
            List<CCPCountry> localCCPCountryList = new ArrayList<>();
            for (String nameCode : countryPreference.split(",")) {
                CCPCountry ccpCountry = CCPCountry.getCountryForNameCodeFromCustomMasterList(getContext(), customMasterCountriesList, getLanguageToApply(), nameCode);
                if (ccpCountry != null) {
                    if (!isAlreadyInList(ccpCountry, localCCPCountryList)) { //to avoid duplicate entry of country
                        localCCPCountryList.add(ccpCountry);
                    }
                }
            }

            if (localCCPCountryList.size() == 0) {
                preferredCountries = null;
            } else {
                preferredCountries = localCCPCountryList;
            }
        }
        if (preferredCountries != null) {
            for (CCPCountry CCPCountry : preferredCountries) {
                CCPCountry.log();
            }
        } else {
        }
    }

    /**
     * this will load preferredCountries based on countryPreference
     */
    void refreshCustomMasterList() {
        if (customMasterCountriesParam == null || customMasterCountriesParam.length() == 0) {
            if (excludedCountriesParam != null && excludedCountriesParam.length() != 0) {
                excludedCountriesParam = excludedCountriesParam.toLowerCase();
                List<CCPCountry> libraryMasterList = CCPCountry.getLibraryMasterCountryList(context, getLanguageToApply());
                List<CCPCountry> localCCPCountryList = new ArrayList<>();
                for (CCPCountry ccpCountry : libraryMasterList) {
                    if (!excludedCountriesParam.contains(ccpCountry.getNameCode().toLowerCase())) {
                        localCCPCountryList.add(ccpCountry);
                    }
                }

                if (localCCPCountryList.size() > 0) {
                    customMasterCountriesList = localCCPCountryList;
                } else {
                    customMasterCountriesList = null;
                }

            } else {
                customMasterCountriesList = null;
            }
        } else {
            List<CCPCountry> localCCPCountryList = new ArrayList<>();
            for (String nameCode : customMasterCountriesParam.split(",")) {
                CCPCountry ccpCountry = CCPCountry.getCountryForNameCodeFromLibraryMasterList(getContext(), getLanguageToApply(), nameCode);
                if (ccpCountry != null) {
                    if (!isAlreadyInList(ccpCountry, localCCPCountryList)) { //to avoid duplicate entry of country
                        localCCPCountryList.add(ccpCountry);
                    }
                }
            }

            if (localCCPCountryList.size() == 0) {
                customMasterCountriesList = null;
            } else {
                customMasterCountriesList = localCCPCountryList;
            }
        }

        if (customMasterCountriesList != null) {
            for (CCPCountry ccpCountry : customMasterCountriesList) {
                ccpCountry.log();
            }
        } else {
        }
    }

    List<CCPCountry> getCustomMasterCountriesList() {
        return customMasterCountriesList;
    }

    /**
     * setCustomMasterCountriesList
     *
     * @param customMasterCountriesList is list of countries that we need as custom master list
     */
    void setCustomMasterCountriesList(List<CCPCountry> customMasterCountriesList) {
        this.customMasterCountriesList = customMasterCountriesList;
    }

    /**
     * getCustomMasterCountriesParam
     *
     * @return comma separated custom master countries' name code. i.e "gb,us,nz,in,pk"
     */
    String getCustomMasterCountriesParam() {
        return customMasterCountriesParam;
    }

    /**
     * To provide definite set of countries when selection dialog is opened.
     * Only custom master countries, if defined, will be there is selection dialog to select from.
     * To set any country in preference, it must be included in custom master countries, if defined
     * When not defined or null or blank is set, it will use library's default master list
     * Custom master list will only limit the visibility of irrelevant country from selection dialog. But all other functions like setCountryForCodeName() or setFullNumber() will consider all the countries.
     *
     * @param customMasterCountriesParam is country name codes separated by comma. e.g. "us,in,nz"
     *                                   if null or "" , will remove custom countries and library default will be used.
     */
    public void setCustomMasterCountries(String customMasterCountriesParam) {
        this.customMasterCountriesParam = customMasterCountriesParam;
    }

    /**
     * This can be used to remove certain countries from the list by keeping all the others.
     * This will be ignored if you have specified your own country master list.
     *
     * @param excludedCountries is country name codes separated by comma. e.g. "us,in,nz"
     *                          null or "" means no country is excluded.
     */
    public void setExcludedCountries(String excludedCountries) {
        this.excludedCountriesParam = excludedCountries;
        refreshCustomMasterList();
    }

    /**
     * isCcpClickable
     *
     * @return true if ccp is enabled for click
     */
    boolean isCcpClickable() {
        return ccpClickable;
    }

    /**
     * Allow click and open dialog
     *
     * @param ccpClickable
     */
    public void setCcpClickable(boolean ccpClickable) {
        this.ccpClickable = ccpClickable;
        if (!ccpClickable) {
            relativeClickConsumer.setClickedListener(null);
            relativeClickConsumer.setClickable(false);
            relativeClickConsumer.setEnabled(false);
        } else {
            relativeClickConsumer.setClickedListener(countryCodeHolderClickListener);
            relativeClickConsumer.setClickable(true);
            relativeClickConsumer.setEnabled(true);
        }
    }

    /**
     * This will match name code of all countries of list against the country's name code.
     *
     * @param CCPCountry
     * @param CCPCountryList list of countries against which country will be checked.
     * @return if country name code is found in list, returns true else return false
     */
    private boolean isAlreadyInList(CCPCountry CCPCountry, List<CCPCountry> CCPCountryList) {
        if (CCPCountry != null && CCPCountryList != null) {
            for (CCPCountry iterationCCPCountry : CCPCountryList) {
                if (iterationCCPCountry.getNameCode().equalsIgnoreCase(CCPCountry.getNameCode())) {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * This function removes possible country code from fullNumber and set rest of the number as carrier number.
     *
     * @param fullNumber combination of country code and carrier number.
     * @param CCPCountry selected country in CCP to detect country code part.
     * @return String
     */
    private String detectCarrierNumber(String fullNumber, CCPCountry CCPCountry) {
        String carrierNumber;
        if (CCPCountry == null || fullNumber == null || fullNumber.isEmpty()) {
            carrierNumber = fullNumber;
        } else {
            int indexOfCode = fullNumber.indexOf(CCPCountry.getPhoneCode());
            if (indexOfCode == -1) {
                carrierNumber = fullNumber;
            } else {
                carrierNumber = fullNumber.substring(indexOfCode + CCPCountry.getPhoneCode().length());
            }
        }
        return carrierNumber;
    }

    /**
     * Related to selected country
     *
     * @param name name
     * @return Language
     */
    private Language getLanguageEnum(String name) {
        if (TextTool.isNullOrEmpty(name)) {
            return Language.ENGLISH;
        }
        return Language.valueOf(name);
    }

    /**
     * getDialogTitle
     *
     * @return If custom text provider is registered, it will return value from provider else default.
     */
    String getDialogTitle() {
        String defaultTitle = CCPCountry.getDialogTitle(context, getLanguageToApply());
        if (customDialogTextProvider != null) {
            return customDialogTextProvider.getCCPDialogTitle(getLanguageToApply(), defaultTitle);
        } else {
            return defaultTitle;
        }
    }

    /**
     * getSearchHintText
     *
     * @return If custom text provider is registered, it will return value from provider else default.
     */
    String getSearchHintText() {
        String defaultHint = CCPCountry.getSearchHintMessage(context, getLanguageToApply());
        if (customDialogTextProvider != null) {
            return customDialogTextProvider.getCCPDialogSearchHintText(getLanguageToApply(), defaultHint);
        } else {
            return defaultHint;
        }
    }

    /**
     * getNoResultACK
     *
     * @return If custom text provider is registered, it will return value from provider else default.
     */
    String getNoResultACK() {
        String defaultNoResultACK = CCPCountry.getNoResultFoundAckMessage(context, getLanguageToApply());
        if (customDialogTextProvider != null) {
            return customDialogTextProvider.getCCPDialogNoResultACK(getLanguageToApply(), defaultNoResultACK);
        } else {
            return defaultNoResultACK;
        }
    }

    /**
     * This method is not encouraged because this might set some other country which have same country code as of yours. e.g 1 is common for US and canada.
     * If you are trying to set US ( and countryPreference is not set) and you pass 1 as @param defaultCountryCode, it will set canada (prior in list due to alphabetical order)
     * Rather use @function setDefaultCountryUsingNameCode("us"); or setDefaultCountryUsingNameCode("US");
     * <p>
     * Default country code defines your default country.
     * Whenever invalid / improper number is found in setCountryForPhoneCode() /  setFullNumber(), it CCP will set to default country.
     * This function will not set default country as selected in CCP. To set default country in CCP call resetToDefaultCountry() right after this call.
     * If invalid defaultCountryCode is applied, it won't be changed.
     *
     * @param defaultCountryCode code of your default country
     *                           if you want to set IN +91(India) as default country, defaultCountryCode =  91
     *                           if you want to set JP +81(Japan) as default country, defaultCountryCode =  81
     */
    @Deprecated
    public void setDefaultCountryUsingPhoneCode(int defaultCountryCode) {
        CCPCountry defaultCCPCountry = CCPCountry.getCountryForCode(getContext(), getLanguageToApply(), preferredCountries, defaultCountryCode); //xml stores data in string format, but want to allow only numeric value to country code to user.
        if (defaultCCPCountry == null) { //if no correct country is found
            //            Log.d(TAG, "No country for code " + defaultCountryCode + " is found");
        } else { //if correct country is found, set the country
            this.defaultCountryCode = defaultCountryCode;
            setDefaultCountry(defaultCCPCountry);
        }
    }

    /**
     * Default country name code defines your default country.
     * Whenever invalid / improper name code is found in setCountryForNameCode(), CCP will set to default country.
     * This function will not set default country as selected in CCP. To set default country in CCP call resetToDefaultCountry() right after this call.
     * If invalid defaultCountryCode is applied, it won't be changed.
     *
     * @param defaultCountryNameCode code of your default country
     *                               if you want to set IN +91(India) as default country, defaultCountryCode =  "IN" or "in"
     *                               if you want to set JP +81(Japan) as default country, defaultCountryCode =  "JP" or "jp"
     */
    public void setDefaultCountryUsingNameCode(String defaultCountryNameCode) {
        CCPCountry defaultCCPCountry = CCPCountry.getCountryForNameCodeFromLibraryMasterList(getContext(), getLanguageToApply(), defaultCountryNameCode); //xml stores data in string format, but want to allow only numeric value to country code to user.
        if (defaultCCPCountry == null) { //if no correct country is found
            //            Log.d(TAG, "No country for nameCode " + defaultCountryNameCode + " is found");
        } else { //if correct country is found, set the country
            this.defaultCountryNameCode = defaultCCPCountry.getNameCode();
            setDefaultCountry(defaultCCPCountry);
        }
    }

    /**
     * getDefaultCountryCode
     *
     * @return String
     * @return: Country Code of default country
     * i.e if default country is IN +91(India)  returns: "91"
     * if default country is JP +81(Japan) returns: "81"
     */
    public String getDefaultCountryCode() {
        return defaultCCPCountry.phoneCode;
    }

    /**
     * * To get code of default country as Integer.
     *
     * @return integer value of default country code in CCP
     * i.e if default country is IN +91(India)  returns: 91
     * if default country is JP +81(Japan) returns: 81
     */
    public int getDefaultCountryCodeAsInt() {
        int code = 0;
        try {
            code = Integer.parseInt(getDefaultCountryCode());
        } catch (Exception e) {
            LogUtils.log("error" + e.getMessage());
        }
        return code;
    }

    /**
     * To get code of default country with prefix "+".
     *
     * @return String value of default country code in CCP with prefix "+"
     * i.e if default country is IN +91(India)  returns: "+91"
     * if default country is JP +81(Japan) returns: "+81"
     */
    public String getDefaultCountryCodeWithPlus() {
        return "+" + getDefaultCountryCode();
    }

    /**
     * To get name of default country.
     *
     * @return String value of country name, default in CCP
     * i.e if default country is IN +91(India)  returns: "India"
     * if default country is JP +81(Japan) returns: "Japan"
     */
    public String getDefaultCountryName() {
        return getDefaultCountry().name;
    }

    /**
     * To get name code of default country.
     *
     * @return String value of country name, default in CCP
     * i.e if default country is IN +91(India)  returns: "IN"
     * if default country is JP +81(Japan) returns: "JP"
     */
    public String getDefaultCountryNameCode() {
        return getDefaultCountry().nameCode.toUpperCase();
    }

    /**
     * reset the default country as selected country.
     */
    public void resetToDefaultCountry() {
        defaultCCPCountry = CCPCountry.getCountryForNameCodeFromLibraryMasterList(getContext(), getLanguageToApply(), getDefaultCountryNameCode());
        setSelectedCountry(defaultCCPCountry);
    }

    /**
     * To get code of selected country.
     *
     * @return String value of selected country code in CCP
     * i.e if selected country is IN +91(India)  returns: "91"
     * if selected country is JP +81(Japan) returns: "81"
     */
    public String getSelectedCountryCode() {
        return getSelectedCountry().phoneCode;
    }

    /**
     * To get code of selected country with prefix "+".
     *
     * @return String value of selected country code in CCP with prefix "+"
     * i.e if selected country is IN +91(India)  returns: "+91"
     * if selected country is JP +81(Japan) returns: "+81"
     */
    public String getSelectedCountryCodeWithPlus() {
        return "+" + getSelectedCountryCode();
    }

    /**
     * * To get code of selected country as Integer.
     *
     * @return integer value of selected country code in CCP
     * i.e if selected country is IN +91(India)  returns: 91
     * if selected country is JP +81(Japan) returns: 81
     */
    public int getSelectedCountryCodeAsInt() {
        int code = 0;
        try {
            code = Integer.parseInt(getSelectedCountryCode());
        } catch (Exception e) {
            LogUtils.log("error" + e.getMessage());
        }
        return code;
    }

    /**
     * To get name of selected country.
     *
     * @return String value of country name, selected in CCP
     * i.e if selected country is IN +91(India)  returns: "India"
     * if selected country is JP +81(Japan) returns: "Japan"
     */
    public String getSelectedCountryName() {
        return getSelectedCountry().name;
    }

    /**
     * To get name of selected country in English.
     *
     * @return String value of country name in English language, selected in CCP
     * i.e if selected country is IN +91(India)  returns: "India" no matter what language is currently selected.
     * if selected country is JP +81(Japan) returns: "Japan"
     */
    public String getSelectedCountryEnglishName() {
        return getSelectedCountry().getEnglishName();
    }

    /**
     * To get name code of selected country.
     *
     * @return String value of country name, selected in CCP
     * i.e if selected country is IN +91(India)  returns: "IN"
     * if selected country is JP +81(Japan) returns: "JP"
     */
    public String getSelectedCountryNameCode() {
        return getSelectedCountry().nameCode.toUpperCase();
    }

    /**
     * To get selected country image resource id
     *
     * @return integer value of the selected country flag reource.
     * For example for georgia it returns R.drawable.flag_georgia
     */
    public int getSelectedCountryFlagResourceId() {
        return getSelectedCountry().flagResID;
    }

    /**
     * This will set country with @param countryCode as country code, in CCP
     *
     * @param countryCode a valid country code.
     *                    If you want to set IN +91(India), countryCode= 91
     *                    If you want to set JP +81(Japan), countryCode= 81
     */
    public void setCountryForPhoneCode(int countryCode) {
        CCPCountry ccpCountry = CCPCountry.getCountryForCode(getContext(), getLanguageToApply(), preferredCountries, countryCode); //xml stores data in string format, but want to allow only numeric value to country code to user.
        if (ccpCountry == null) {
            if (defaultCCPCountry == null) {
                defaultCCPCountry = CCPCountry.getCountryForCode(getContext(), getLanguageToApply(), preferredCountries, defaultCountryCode);
            }
            setSelectedCountry(defaultCCPCountry);
        } else {
            setSelectedCountry(ccpCountry);
        }
    }

    /**
     * This will set country with @param countryNameCode as country name code, in CCP
     *
     * @param countryNameCode a valid country name code.
     *                        If you want to set IN +91(India), countryCode= IN
     *                        If you want to set JP +81(Japan), countryCode= JP
     */
    public void setCountryForNameCode(String countryNameCode) {
        CCPCountry country = CCPCountry.getCountryForNameCodeFromLibraryMasterList(getContext(), getLanguageToApply(), countryNameCode); //xml stores data in string format, but want to allow only numeric value to country code to user.
        if (country == null) {
            if (defaultCCPCountry == null) {
                defaultCCPCountry = CCPCountry.getCountryForCode(getContext(), getLanguageToApply(), preferredCountries, defaultCountryCode);
            }
            setSelectedCountry(defaultCCPCountry);
        } else {
            setSelectedCountry(country);
        }
    }

    /**
     * All functions that work with fullNumber need an editText to write and read carrier number of full number.
     * An editText for carrier number must be registered in order to use functions like setFullNumber() and getFullNumber().
     *
     * @param editTextCarrierNumber - an editText where user types carrier number ( the part of full number other than country code).
     */
    public void registerCarrierNumberEditText(TextField editTextCarrierNumber) {
        setEditText_registeredCarrierNumber(editTextCarrierNumber);
    }

    /**
     * If edittext was already registered, this will remove attached textwatchers and set
     * editText to null
     */
    public void deregisterCarrierNumberEditText() {
        if (editText_registeredCarrierNumber != null) {
            // remove validity listener
            try {
                editText_registeredCarrierNumber.removeTextObserver(validityTextWatcher);
            } catch (Exception ignored) {
                LogUtils.log("error" + ignored.getMessage());
            }

            // if possible, remove formatting textwatcher
            try {
                editText_registeredCarrierNumber.removeTextObserver(formattingTextWatcher);
            } catch (Exception ignored) {
                LogUtils.log("error" + ignored.getMessage());
            }

            editText_registeredCarrierNumber.setHint("");

            editText_registeredCarrierNumber = null;
        }
    }

    private Phonenumber.PhoneNumber getEnteredPhoneNumber() throws NumberParseException {
        String carrierNumber = "";
        if (editText_registeredCarrierNumber != null) {
            carrierNumber = PhoneNumberUtil.normalizeDigitsOnly(editText_registeredCarrierNumber.getText().toString());
        }
        return getPhoneUtil().parse(carrierNumber, getSelectedCountryNameCode());
    }

    /**
     * This function combines selected country code from CCP and carrier number from @param editTextCarrierNumber
     *
     * @return Full number is countryCode + carrierNumber i.e countryCode= 91 and carrier number= 8866667722, this will return "918866667722"
     */
    public String getFullNumber() {
        try {
            Phonenumber.PhoneNumber phoneNumber = getEnteredPhoneNumber();
            return getPhoneUtil().format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.E164).substring(1);
        } catch (NumberParseException e) {
            Log.e(TAG, "getFullNumber: Could not parse number");
            return getSelectedCountryCode() + PhoneNumberUtil.normalizeDigitsOnly(editText_registeredCarrierNumber.getText().toString());
        }
    }

    /**
     * Separate out country code and carrier number from fullNumber.
     * Sets country of separated country code in CCP and carrier number as text of editTextCarrierNumber
     * If no valid country code is found from full number, CCP will be set to default country code and full number will be set as carrier number to editTextCarrierNumber.
     *
     * @param fullNumber is combination of country code and carrier number, (country_code+carrier_number) for example if country is India (+91) and carrier/mobile number is 8866667722 then full number will be 9188666667722 or +918866667722. "+" in starting of number is optional.
     */
    public void setFullNumber(String fullNumber) {
        CCPCountry country = CCPCountry.getCountryForNumber(getContext(), getLanguageToApply(), preferredCountries, fullNumber);
        if (country == null)
            country = getDefaultCountry();
        setSelectedCountry(country);
        String carrierNumber = detectCarrierNumber(fullNumber, country);
        if (getEditText_registeredCarrierNumber() != null) {
            getEditText_registeredCarrierNumber().setText(carrierNumber);
            updateFormattingTextWatcher();
        } else {
            Log.e(TAG, "EditText for carrier number is not registered. Register it using registerCarrierNumberEditText() before getFullNumber() or setFullNumber().");
        }
    }

    /**
     * This function combines selected country code from CCP and carrier number from @param editTextCarrierNumber
     * This will return formatted number.
     *
     * @return Full number is countryCode + carrierNumber i.e countryCode= 91 and carrier number= 8866667722, this will return "918866667722"
     */
    public String getFormattedFullNumber() {
        try {
            Phonenumber.PhoneNumber phoneNumber = getEnteredPhoneNumber();
            return "+" + getPhoneUtil().format(phoneNumber, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL).substring(1);
        } catch (NumberParseException e) {
            Log.e(TAG, "getFullNumber: Could not parse number");
            return getFullNumberWithPlus();
        }
    }

    /**
     * This function combines selected country code from CCP and carrier number from @param editTextCarrierNumber and prefix "+"
     *
     * @return Full number is countryCode + carrierNumber i.e countryCode= 91 and carrier number= 8866667722, this will return "+918866667722"
     */
    public String getFullNumberWithPlus() {
        return "+" + getFullNumber();
    }

    /**
     * getContentColor
     *
     * @return content color of CCP's text and small downward arrow.
     */
    public int getContentColor() {
        return contentColor;
    }

    /**
     * Sets text and small down arrow color of CCP.
     *
     * @param contentColor color to apply to text and down arrow
     */
    public void setContentColor(int contentColor) {
        this.contentColor = contentColor;
        textView_selectedCountry.setTextColor(new Color(contentColor));

        //change arrow color only if explicit arrow color is not specified.
        if (this.arrowColor == DEFAULT_UNSET) {
            try {
                Resource resource = getResourceManager().getResource(ResourceTable.Media_icon_arrow_down);
                PixelMap pixelmap = ImageSource.create(resource, null).createPixelmap(null);
                PixelMapElement pixelMapElement = new PixelMapElement(pixelmap);
                ImageTint.setImageTint(imageViewArrow, pixelMapElement, contentColor, BlendMode.SRC_IN);

            } catch (Exception e) {
                LogUtils.log("error" + e.getMessage());
            }
        }
    }

    /**
     * set small down arrow color of CCP.
     *
     * @param arrowColor color to apply to text and down arrow
     */
    public void setArrowColor(int arrowColor) {
        this.arrowColor = arrowColor;
        if (this.arrowColor == DEFAULT_UNSET) {
            if (contentColor != DEFAULT_UNSET) {
                try {
                    Resource resource = getResourceManager().getResource(ResourceTable.Media_icon_arrow_down);
                    PixelMap pixelmap = ImageSource.create(resource, null).createPixelmap(null);
                    PixelMapElement pixelMapElement = new PixelMapElement(pixelmap);
                    ImageTint.setImageTint(imageViewArrow, pixelMapElement, this.contentColor, BlendMode.SRC_IN);
                } catch (Exception e) {
                    LogUtils.log("error" + e.getMessage());
                }
            }
        } else {
            try {
                Resource resource = getResourceManager().getResource(ResourceTable.Media_icon_arrow_down);
                PixelMap pixelmap = ImageSource.create(resource, null).createPixelmap(null);
                PixelMapElement pixelMapElement = new PixelMapElement(pixelmap);
                ImageTint.setImageTint(imageViewArrow, pixelMapElement, this.arrowColor, BlendMode.SRC_IN);
            } catch (Exception e) {
                LogUtils.log("error" + e.getMessage());
            }
        }
    }

    /**
     * Sets flag border color of CCP.
     *
     * @param borderFlagColor color to apply to flag border
     */
    public void setFlagBorderColor(int borderFlagColor) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(borderFlagColor));
        linearFlagBorder.setBackground(shapeElement);
    }

    /**
     * Modifies size of text in side CCP view.
     *
     * @param textSize size of text in pixels
     */
    public void setTextSize(int textSize) {
        if (textSize > 0) {
            textView_selectedCountry.setTextSize(textSize, Text.TextSizeType.PX);
            setArrowSize(textSize);
            setFlagSize(textSize);
        }
    }

    /**
     * Modifies size of downArrow in CCP view
     *
     * @param arrowSize size in pixels
     */
    public void setArrowSize(int arrowSize) {
        if (arrowSize > 0) {
            LayoutConfig params = (LayoutConfig) imageViewArrow.getLayoutConfig();
            params.width = arrowSize;
            params.height = arrowSize;
            imageViewArrow.setLayoutConfig(params);

            //调整视图的宽高后，需要重新剪切图片
            try {
                int newSize = (int) (SystemUtils.px2vp(getContext(), arrowSize) * 1.5);
                int p = (arrowSize - newSize) / 2;
                //设置Padding ，让图片内容居中
                imageViewArrow.setPadding(p, p, p, p);

                Resource resource = getResourceManager().getResource(ResourceTable.Media_icon_arrow_down);
                ImageSource.DecodingOptions options = new ImageSource.DecodingOptions();
                Size s = new Size();
                s.width = newSize;
                s.height = newSize;
                options.desiredSize = s;

                PixelMap pixelmap = ImageSource.create(resource, null).createPixelmap(options);
                PixelMapElement pixelMapElement = new PixelMapElement(pixelmap);

                int color = this.arrowColor;
                if (color == DEFAULT_UNSET) {
                    color = this.contentColor;
                }
                if (color == DEFAULT_UNSET) {
                    ImageTint.setImageElement(imageViewArrow, pixelMapElement);
                } else {
                    ImageTint.setImageTint(imageViewArrow, pixelMapElement, color);
                }
            } catch (Exception e) {
                LogUtils.log("error" + e.getMessage());
            }


        }
    }

    /**
     * If nameCode of country in CCP view is not required use this to show/hide country name code of ccp view.
     *
     * @param showNameCode true will show country name code in ccp view, it will result " (IN) +91 "
     *                     false will remove country name code from ccp view, it will result  " +91 "
     */
    public void showNameCode(boolean showNameCode) {
        this.showNameCode = showNameCode;
        setSelectedCountry(selectedCCPCountry);
    }

    /**
     * This can change visility of arrow.
     *
     * @param showArrow true will show arrow and false will hide arrow from there.
     */
    public void showArrow(boolean showArrow) {
        this.showArrow = showArrow;
        refreshArrowViewVisibility();
    }

    /**
     * This will set preferred countries using their name code. Prior preferred countries will be replaced by these countries.
     * Preferred countries will be at top of country selection box.
     * If more than one countries have same country code, country in preferred list will have higher priory than others. e.g. Canada and US have +1 as their country code. If "us" is set as preferred country then US will be selected whenever setCountryForPhoneCode(1); or setFullNumber("+1xxxxxxxxx"); is called.
     *
     * @param countryPreference is country name codes separated by comma. e.g. "us,in,nz"
     */
    public void setCountryPreference(String countryPreference) {
        this.countryPreference = countryPreference;
    }

    /**
     * Language will be applied to country select dialog
     * If autoDetectCountry is true, ccp will try to detect language from locale.
     * Detected language is supported If no language is detected or detected language is not supported by ccp, it will set default language as set.
     *
     * @param language
     */
    public void changeDefaultLanguage(Language language) {
        setCustomDefaultLanguage(language);
    }

    /**
     * To change font of ccp views
     *
     * @param typeFace
     */
    public void setTypeFace(Font typeFace) {
        try {
            //  textView_selectedCountry.setTypeface(typeFace);
            textView_selectedCountry.setFont(typeFace);

            setDialogTypeFace(typeFace);
        } catch (Exception e) {
            LogUtils.log("error" + e.getMessage());
        }
    }

    /**
     * To change font of ccp views along with style.
     *
     * @param typeFace
     * @param style
     */
    public void setDialogTypeFace(Font typeFace, int style) {
        try {
            dialogTypeFace = typeFace;
        } catch (Exception e) {
            LogUtils.log("error" + e.getMessage());
        }
    }

    /**
     * To change font of ccp views along with style.
     *
     * @param typeFace
     * @param style
     */
    public void setTypeFace(Font typeFace, int style) {
        try {
            textView_selectedCountry.setFont(typeFace);
            setDialogTypeFace(typeFace, style);
        } catch (Exception e) {
            LogUtils.log("error" + e.getMessage());
        }
    }

    /**
     * To get call back on country selection a onCountryChangeListener must be registered.
     *
     * @param onCountryChangeListener
     */
    public void setOnCountryChangeListener(OnCountryChangeListener onCountryChangeListener) {
        this.onCountryChangeListener = onCountryChangeListener;
    }

    /**
     * Modifies size of flag in CCP view
     *
     * @param flagSize size in pixels
     */
    public void setFlagSize(int flagSize) {
        //移动到设置图片的地方
        if (flagSize == 0) {
            flagSize = (int) SystemUtils.vp2px(getContext(), 18);
        }
        this.flagSize = flagSize;
    }


    public void showFlag(boolean showFlag) {
        this.showFlag = showFlag;
        refreshFlagVisibility();
        // if (!isInEditMode())
        setSelectedCountry(selectedCCPCountry);
    }

    private void refreshFlagVisibility() {
        if (showFlag) {
            if (ccpUseEmoji) {
                linearFlagHolder.setVisibility(HIDE);
            } else {
                linearFlagHolder.setVisibility(VISIBLE);
            }
        } else {
            linearFlagHolder.setVisibility(HIDE);
        }
    }

    public void useFlagEmoji(boolean useFlagEmoji) {
        this.ccpUseEmoji = useFlagEmoji;
        refreshFlagVisibility();
        setSelectedCountry(selectedCCPCountry);
    }

    public void showFullName(boolean showFullName) {
        this.showFullName = showFullName;
        setSelectedCountry(selectedCCPCountry);
    }

    /**
     * SelectionDialogSearch is the facility to search through the list of country while selecting.
     *
     * @return true if search is set allowed
     */
    public boolean isSearchAllowed() {
        return searchAllowed;
    }

    /**
     * SelectionDialogSearch is the facility to search through the list of country while selecting.
     *
     * @param searchAllowed true will allow search and false will hide search box
     */
    public void setSearchAllowed(boolean searchAllowed) {
        this.searchAllowed = searchAllowed;
    }

    /**
     * Sets validity change listener.
     * First call back will be sent right away.
     *
     * @param phoneNumberValidityChangeListener
     */
    public void setPhoneNumberValidityChangeListener(PhoneNumberValidityChangeListener phoneNumberValidityChangeListener) {
        this.phoneNumberValidityChangeListener = phoneNumberValidityChangeListener;
        if (editText_registeredCarrierNumber != null && phoneNumberValidityChangeListener != null) {
            reportedValidity = isValidFullNumber();
            phoneNumberValidityChangeListener.onValidityChanged(reportedValidity);
        }
    }

    /**
     * Sets failure listener.
     *
     * @param failureListener
     */
    public void setAutoDetectionFailureListener(FailureListener failureListener) {
        this.failureListener = failureListener;
    }

    /**
     * If developer wants to change CCP Dialog's Title, Search Hint text or no result ACK,
     * a custom dialog text provider should be set.
     *
     * @param customDialogTextProvider
     */
    public void setCustomDialogTextProvider(CustomDialogTextProvider customDialogTextProvider) {
        this.customDialogTextProvider = customDialogTextProvider;
    }

    /**
     * Opens country selection dialog.
     * By default this is called from ccp click.
     * Developer can use this to trigger manually.
     */
    public void launchCountrySelectionDialog() {
        launchCountrySelectionDialog(null);
    }

    /**
     * Manually trigger selection dialog and set
     * scroll position to specified country.
     *
     * @param countryNameCode
     */
    public void launchCountrySelectionDialog(final String countryNameCode) {
        CountryCodeDialog.openCountryCodeDialog(codePicker, countryNameCode);
    }

    /**
     * This function will check the validity of entered number.
     * It will use PhoneNumberUtil to check validity
     *
     * @return true if entered carrier number is valid else false
     */
    public boolean isValidFullNumber() {
        try {
            if (getEditText_registeredCarrierNumber() != null && getEditText_registeredCarrierNumber().getText().length() != 0) {
                Phonenumber.PhoneNumber phoneNumber = getPhoneUtil().parse("+" + selectedCCPCountry.getPhoneCode() + getEditText_registeredCarrierNumber().getText(), selectedCCPCountry.getNameCode());
                return getPhoneUtil().isValidNumber(phoneNumber);
            } else if (getEditText_registeredCarrierNumber() == null) {
                Toast.showShort(context, "No editText for Carrier number found.");
                return false;
            } else {
                return false;
            }
        } catch (NumberParseException e) {
            //            when number could not be parsed, its not valid
            return false;
        }
    }

    private PhoneNumberUtil getPhoneUtil() {
        if (phoneUtil == null) {

            phoneUtil = PhoneNumberUtil.createInstance(context);
        }
        return phoneUtil;
    }

    /**
     * loads current country in ccp using locale and telephony manager
     * this will follow specified order in countryAutoDetectionPref
     *
     * @param loadDefaultWhenFails if all of pref methods fail to detect country then should this
     *                             function load default country or not is decided with this flag
     */
    public void setAutoDetectedCountry(boolean loadDefaultWhenFails) {
        try {
            boolean successfullyDetected = false;
            for (int i = 0; i < selectedAutoDetectionPref.representation.length(); i++) {
                switch (selectedAutoDetectionPref.representation.charAt(i)) {
                    case '1':
                        successfullyDetected = detectSIMCountry(false);
                        break;
                    case '2':
                        successfullyDetected = detectNetworkCountry(false);
                        break;
                    case '3':
                        successfullyDetected = detectLocaleCountry(false);
                        break;
                }
                if (successfullyDetected) {
                    break;
                } else {
                    if (failureListener != null) {
                        failureListener.onCountryAutoDetectionFailed();
                    }
                }
            }

            if (!successfullyDetected && loadDefaultWhenFails) {
                resetToDefaultCountry();
            }
        } catch (Exception e) {
            Log.e(TAG, "setAutoDetectCountry: Exception" + e.getMessage());
            if (loadDefaultWhenFails) {
                resetToDefaultCountry();
            }
        }
    }

    /**
     * This will detect country from SIM info and then load it into CCP.
     *
     * @param loadDefaultWhenFails true if want to reset to default country when sim country cannot be detected. if false, then it
     *                             will not change currently selected country
     * @return true if it successfully sets country, false otherwise
     */
    public boolean detectSIMCountry(boolean loadDefaultWhenFails) {
        try {
            SimInfoManager telephonyManager = SimInfoManager.getInstance(context);
            String simCountryISO = telephonyManager.getIsoCountryCodeForSim(0);
            if (simCountryISO == null || simCountryISO.isEmpty()) {
                if (loadDefaultWhenFails) {
                    resetToDefaultCountry();
                }
                return false;
            }
            setSelectedCountry(CCPCountry.getCountryForNameCodeFromLibraryMasterList(getContext(), getLanguageToApply(), simCountryISO));
            return true;
        } catch (Exception e) {
            if (loadDefaultWhenFails) {
                resetToDefaultCountry();
            }
            return false;
        }
    }

    /**
     * This will detect country from NETWORK info and then load it into CCP.
     *
     * @param loadDefaultWhenFails true if want to reset to default country when network country cannot be detected. if false, then it
     *                             will not change currently selected country
     * @return true if it successfully sets country, false otherwise
     */
    public boolean detectNetworkCountry(boolean loadDefaultWhenFails) {
        try {
            RadioInfoManager telephonyManager = RadioInfoManager.getInstance(context);
            String networkCountryISO = telephonyManager.getIsoCountryCodeForNetwork(0);
            if (networkCountryISO == null || networkCountryISO.isEmpty()) {
                if (loadDefaultWhenFails) {
                    resetToDefaultCountry();
                }
                return false;
            }
            setSelectedCountry(CCPCountry.getCountryForNameCodeFromLibraryMasterList(getContext(), getLanguageToApply(), networkCountryISO));
            return true;
        } catch (Exception e) {
            if (loadDefaultWhenFails) {
                resetToDefaultCountry();
            }
            return false;
        }
    }

    /**
     * This will detect country from LOCALE info and then load it into CCP.
     *
     * @param loadDefaultWhenFails true if want to reset to default country when locale country cannot be detected. if false, then it
     *                             will not change currently selected country
     * @return true if it successfully sets country, false otherwise
     */
    public boolean detectLocaleCountry(boolean loadDefaultWhenFails) {
        try {
            String localeCountryISO = context.getResourceManager().getConfiguration().getFirstLocale().getCountry();
            if (localeCountryISO == null || localeCountryISO.isEmpty()) {
                if (loadDefaultWhenFails) {
                    resetToDefaultCountry();
                }
                return false;
            }
            setSelectedCountry(CCPCountry.getCountryForNameCodeFromLibraryMasterList(getContext(), getLanguageToApply(), localeCountryISO));
            return true;
        } catch (Exception e) {
            if (loadDefaultWhenFails) {
                resetToDefaultCountry();
            }
            return false;
        }
    }

    /**
     * This will update the pref for country auto detection.
     * Remeber, this will not call setAutoDetectedCountry() to update country. This must be called separately.
     *
     * @param selectedAutoDetectionPref new detection pref
     */
    public void setCountryAutoDetectionPref(AutoDetectionPref selectedAutoDetectionPref) {
        this.selectedAutoDetectionPref = selectedAutoDetectionPref;
    }

    protected void onUserTappedCountry(CCPCountry CCPCountry) {
        if (codePicker.rememberLastSelection) {
            codePicker.storeSelectedCountryNameCode(CCPCountry.getNameCode());
        }
        setSelectedCountry(CCPCountry);
    }

    public void setDetectCountryWithAreaCode(boolean detectCountryWithAreaCode) {
        this.detectCountryWithAreaCode = detectCountryWithAreaCode;
        updateFormattingTextWatcher();
    }

    public void setHintExampleNumberEnabled(boolean hintExampleNumberEnabled) {
        this.hintExampleNumberEnabled = hintExampleNumberEnabled;
        updateHint();
    }

    public void setHintExampleNumberType(PhoneNumberType hintExampleNumberType) {
        this.hintExampleNumberType = hintExampleNumberType;
        updateHint();
    }

    public boolean isDialogInitialScrollToSelectionEnabled() {
        return ccpDialogInitialScrollToSelection;
    }

    public void setTalkBackTextProvider(CCPTalkBackTextProvider talkBackTextProvider) {
        this.talkBackTextProvider = talkBackTextProvider;
        setSelectedCountry(selectedCCPCountry);
    }

    /**
     * This will decide initial scroll position of countries list in dialog.
     *
     * @param initialScrollToSelection : false -> show list without any scroll
     *                                 true -> will scroll to the position of the selected country.
     *                                 Note: if selected country is a preferred country,
     *                                 then it will not scroll and show full preferred countries list.
     */
    public void enableDialogInitialScrollToSelection(boolean initialScrollToSelection) {
        this.ccpDialogInitialScrollToSelection = initialScrollToSelection;
    }

    /**
     * To listen to the click handle action manually,
     * a custom clicklistener must be set.
     * This will override the default click listener which opens the selection dialog.
     *
     * @param clickListener will start receiving click callbacks. If null then default click listener
     *                      will receive callback and selection dialog will be prompted.
     */
    public void overrideClickListener(ClickedListener clickListener) {
        customClickListener = clickListener;
    }


    @Override
    public void onComponentBoundToWindow(Component component) {

    }

    @Override
    public void onComponentUnboundFromWindow(Component component) {
        CountryCodeDialog.clear();
    }

    private boolean isInEditMode() {
        return false;
    }


    /**
     * Update every time new language is supported #languageSupport
     */
    //add an entry for your language in attrs.xml's <attr name="language" format="enum"> enum.
    //add here so that language can be set programmatically
    public enum Language {
        AFRIKAANS("af"),
        ARABIC("ar"),
        BENGALI("bn"),
        CHINESE_SIMPLIFIED("zh", "CN", "Hans"),
        CHINESE_TRADITIONAL("zh", "TW", "Hant"),
        CZECH("cs"),
        DANISH("da"),
        DUTCH("nl"),
        ENGLISH("en"),
        FARSI("fa"),
        FRENCH("fr"),
        GERMAN("de"),
        GREEK("el"),
        GUJARATI("gu"),
        HEBREW("iw"),
        HINDI("hi"),
        INDONESIA("in"),
        ITALIAN("it"),
        JAPANESE("ja"),
        KAZAKH("kk"),
        KOREAN("ko"),
        MARATHI("mr"),
        POLISH("pl"),
        PORTUGUESE("pt"),
        PUNJABI("pa"),
        RUSSIAN("ru"),
        SLOVAK("sk"),
        SLOVENIAN("si"),
        SPANISH("es"),
        SWEDISH("sv"),
        TAGALOG("tl"),
        TURKISH("tr"),
        UKRAINIAN("uk"),
        URDU("ur"),
        UZBEK("uz"),
        VIETNAMESE("vi");

        private String code;
        private String country;
        private String script;

        Language(String code, String country, String script) {
            this.code = code;
            this.country = country;
            this.script = script;
        }

        Language(String code) {
            this.code = code;
        }

        public static Language forCountryNameCode(String code) {
            Language lang = Language.ENGLISH;
            for (Language language : Language.values()) {
                if (language.code.equals(code)) {
                    lang = language;
                }
            }
            return lang;
        }

        public String getCode() {
            return code;
        }

        public void setCode(String code) {
            this.code = code;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }

        public String getScript() {
            return script;
        }

        public void setScript(String script) {
            this.script = script;
        }
    }

    public enum PhoneNumberType {
        MOBILE,
        FIXED_LINE,
        // In some regions (e.g. the USA), it is impossible to distinguish between fixed-line and
        // mobile numbers by looking at the phone number itself.
        FIXED_LINE_OR_MOBILE,
        // Freephone lines
        TOLL_FREE,
        PREMIUM_RATE,
        // The cost of this call is shared between the caller and the recipient, and is hence typically
        // less than PREMIUM_RATE calls. See // http://en.wikipedia.org/wiki/Shared_Cost_Service for
        // more information.
        SHARED_COST,
        // Voice over IP numbers. This includes TSoIP (Telephony Service over IP).
        VOIP,
        // A personal number is associated with a particular person, and may be routed to either a
        // MOBILE or FIXED_LINE number. Some more information can be found here:
        // http://en.wikipedia.org/wiki/Personal_Numbers
        PERSONAL_NUMBER,
        PAGER,
        // Used for "Universal Access Numbers" or "Company Numbers". They may be further routed to
        // specific offices, but allow one number to be used for a company.
        UAN,
        // Used for "Voice Mail Access Numbers".
        VOICEMAIL,
        // A phone number is of type UNKNOWN when it does not fit any of the known patterns for a
        // specific region.
        UNKNOWN
    }

    public enum AutoDetectionPref {
        SIM_ONLY("1"),
        NETWORK_ONLY("2"),
        LOCALE_ONLY("3"),
        SIM_NETWORK("12"),
        NETWORK_SIM("21"),
        SIM_LOCALE("13"),
        LOCALE_SIM("31"),
        NETWORK_LOCALE("23"),
        LOCALE_NETWORK("32"),
        SIM_NETWORK_LOCALE("123"),
        SIM_LOCALE_NETWORK("132"),
        NETWORK_SIM_LOCALE("213"),
        NETWORK_LOCALE_SIM("231"),
        LOCALE_SIM_NETWORK("312"),
        LOCALE_NETWORK_SIM("321");

        String representation;

        AutoDetectionPref(String representation) {
            this.representation = representation;
        }

        public static AutoDetectionPref getPrefForValue(String value) {
            for (AutoDetectionPref autoDetectionPref : AutoDetectionPref.values()) {
                if (autoDetectionPref.representation.equals(value)) {
                    return autoDetectionPref;
                }
            }
            return SIM_NETWORK_LOCALE;
        }
    }

    /**
     * When width is "match_parent", this gravity will decide the placement of text.
     */
    public enum TextGravity {
        LEFT(-1), CENTER(0), RIGHT(1);

        int enumIndex;

        TextGravity(int i) {
            enumIndex = i;
        }
    }

    /**
     * interface to set change listener
     */
    public interface OnCountryChangeListener {
        void onCountrySelected();
    }

    /**
     * interface to listen to failure events
     */
    public interface FailureListener {
        //when country auto detection failed.
        void onCountryAutoDetectionFailed();
    }

    /**
     * Interface to check phone number validity change listener
     */
    public interface PhoneNumberValidityChangeListener {
        void onValidityChanged(boolean isValidNumber);
    }

    public interface DialogEventsListener {
        void onCcpDialogOpen(BaseDialog dialog);

        void onCcpDialogDismiss(IDialog dialogInterface);

        void onCcpDialogCancel(IDialog dialogInterface);
    }

    public interface CustomDialogTextProvider {
        String getCCPDialogTitle(Language language, String defaultTitle);

        String getCCPDialogSearchHintText(Language language, String defaultSearchHintText);

        String getCCPDialogNoResultACK(Language language, String defaultNoResultACK);
    }

}
