/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.hbb20;

import com.hbb20.utils.SystemUtils;

import ohos.agp.components.AttrSet;
import ohos.agp.components.Component;
import ohos.agp.components.StackLayout;
import ohos.app.Context;

/**
 * KeyboardView.
 *
 * @author author
 * @version version
 */
public class KeyboardView extends StackLayout implements Component.LayoutRefreshedListener {
    private int componentHeight = 0;
    private boolean showKeyboard = false;

    public KeyboardView(Context context) {
        this(context, null);
    }

    public KeyboardView(Context context, AttrSet attrSet) {
        this(context, attrSet, "");
    }

    public KeyboardView(Context context, AttrSet attrSet, String styleName) {
        super(context, attrSet, styleName);
        initView();
    }

    private void initView() {
        setLayoutRefreshedListener(this);
    }


    @Override
    public void onRefreshed(Component component) {
        int currentHeight = component.getHeight();
        this.componentHeight = Math.max(currentHeight, componentHeight);
        if (showKeyboard) {
            if (componentHeight == currentHeight) {
                this.showKeyboard = false;
                KeyboardManager.get().dispatchListener(this.showKeyboard, 0);
            }
        } else {
            int height = this.componentHeight - currentHeight;
            if (height >= SystemUtils.vp2px(getContext(), 200)) {
                this.showKeyboard = true;
                KeyboardManager.get().dispatchListener(this.showKeyboard, height);
            }
        }
    }
}
