package com.futuremind.recyclerviewfastscroll.viewprovider;

/**
 * DefaultBubbleBehavior
 *
 * @author author
 * @version version
 */
public class DefaultBubbleBehavior implements ViewBehavior {
    /**
     * animationManager
     */
    private final VisibilityAnimationManager animationManager;

    /**
     * DefaultBubbleBehavior
     *
     * @param animationManager animationManager
     */
    public DefaultBubbleBehavior(VisibilityAnimationManager animationManager) {
        this.animationManager = animationManager;
    }

    /**
     * onHandleGrabbed
     */
    @Override
    public void onHandleGrabbed() {
        animationManager.show();
    }

    /**
     * onHandleReleased
     */
    @Override
    public void onHandleReleased() {
        animationManager.hide();
    }

    /**
     * onScrollStarted
     */
    @Override
    public void onScrollStarted() {

    }

    /**
     * onScrollFinished
     */
    @Override
    public void onScrollFinished() {

    }

}
