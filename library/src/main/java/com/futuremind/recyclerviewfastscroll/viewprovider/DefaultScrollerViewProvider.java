package com.futuremind.recyclerviewfastscroll.viewprovider;

import com.futuremind.recyclerviewfastscroll.Utils;
import com.hbb20.ResourceTable;
import com.hbb20.utils.SystemUtils;
import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;

import static ohos.agp.components.element.ShapeElement.LINEAR_GRADIENT_SHADER_TYPE;

/**
 * Created by Michal on 05/08/16.
 */
public class DefaultScrollerViewProvider extends ScrollerViewProvider {

    protected Component bubble;
    protected StackLayout handle;

    @Override
    public Component provideHandleView(ComponentContainer container) {
        handle = new StackLayout(getContext());


        int handleWidth = getScroller().isVertical() ? 24 : 30;
        int handleHeight = getScroller().isVertical() ? 30 : 24;

        Component handleChild = new Component(getContext());

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setCornerRadius(SystemUtils.vp2px(getContext(), 3));
        shapeElement.setRgbColor(new RgbColor(0xaa, 0xaa, 0xaa));
        StackLayout.LayoutConfig layoutConfig = new StackLayout.LayoutConfig();
        if (getScroller().isVertical()) {
            layoutConfig.width = (int) SystemUtils.vp2px(getContext(), 6);
            layoutConfig.height = (int) SystemUtils.vp2px(getContext(), handleHeight);
        } else {
            layoutConfig.width = (int) SystemUtils.vp2px(getContext(), handleWidth);
            layoutConfig.height = (int) SystemUtils.vp2px(getContext(), 6);
        }
        handleChild.setLayoutConfig(layoutConfig);
        Utils.setBackground(handleChild, shapeElement);
        handle.addComponent(handleChild);

        DirectionalLayout.LayoutConfig params = new DirectionalLayout.LayoutConfig((int) SystemUtils.vp2px(getContext(), handleWidth), (int) SystemUtils.vp2px(getContext(), handleHeight));
        //right
        params.alignment = 5;
        handle.setLayoutConfig(params);
        int padding = (int) SystemUtils.vp2px(getContext(), 9);
        int paddingOff = (int) SystemUtils.vp2px(getContext(), 4);
        if (getScroller().isVertical()) {
            handle.setPadding(padding + paddingOff, 0, padding - paddingOff, 0);
        } else {
            handle.setPadding(0, padding, 0, padding);
        }
        return handle;
    }

    @Override
    public Component provideBubbleView(ComponentContainer container) {
        bubble = LayoutScatter.getInstance(getContext()).parse(ResourceTable.Layout_fastscroll__default_bubble, container, false);
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(new RgbColor(0xff, 0xff, 0xff));
        float r = SystemUtils.vp2px(container.getContext(), 20);
        float[] radii = new float[]{r, r, r, r, 0, 0, r, r,};
        shapeElement.setCornerRadiiArray(radii);
        shapeElement.setStroke(1, new RgbColor(0xaa, 0xaa, 0xaa));
        shapeElement.setShaderType(LINEAR_GRADIENT_SHADER_TYPE);
        bubble.setBackground(shapeElement);
        return bubble;
    }

    @Override
    public Text provideBubbleTextView() {
        return (Text) bubble;
    }

    @Override
    public int getBubbleOffset() {
        return (int) (getScroller().isVertical() ? (handle.getHeight() / 2.0) - bubble.getHeight() : (handle.getWidth() / 2.0) - bubble.getWidth());
    }

    @Override
    protected ViewBehavior provideHandleBehavior() {
        return null;
    }

    @Override
    protected ViewBehavior provideBubbleBehavior() {
        return new DefaultBubbleBehavior(new VisibilityAnimationManager.Builder(bubble).withPivotX(1f).withPivotY(1f).build());
    }


}
