package com.futuremind.recyclerviewfastscroll.viewprovider;

/**
 * Created by Michal on 11/08/16.
 * Extending classes should use this interface to get notified about events that occur to the
 * fastscroller elements (handle and bubble) and react accordingly. See {@link DefaultBubbleBehavior}
 * for an example.
 *
 * @author a
 * @version 1.0
 */
public interface ViewBehavior {
    /**
     * onHandleGrabbed
     */
    void onHandleGrabbed();

    /**
     * onHandleReleased
     */
    void onHandleReleased();

    /**
     * onScrollStarted
     */
    void onScrollStarted();

    /**
     * onScrollFinished
     */
    void onScrollFinished();
}
