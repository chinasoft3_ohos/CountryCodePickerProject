package com.futuremind.recyclerviewfastscroll.viewprovider;

import com.futuremind.recyclerviewfastscroll.FastScroller;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.Text;
import ohos.app.Context;

/**
 * Created by Michal on 05/08/16.
 * Provides {@link Component}s and their behaviors for the handle and bubble of the fastscroller.
 */
public abstract class ScrollerViewProvider {

    private FastScroller scroller;
    private ViewBehavior handleBehavior;
    private ViewBehavior bubbleBehavior;

    public void setFastScroller(FastScroller scroller) {
        this.scroller = scroller;
    }

    protected Context getContext() {
        return scroller.getContext();
    }

    protected FastScroller getScroller() {
        return scroller;
    }

    /**
     * provideHandleView
     *
     * @param container The container {@link FastScroller} for the view to inflate properly.
     * @return A view which will be by the {@link FastScroller} used as a handle.
     */
    public abstract Component provideHandleView(ComponentContainer container);

    /**
     * provideBubbleView
     *
     * @param container The container {@link FastScroller} for the view to inflate properly.
     * @return A view which will be by the {@link FastScroller} used as a bubble.
     */
    public abstract Component provideBubbleView(ComponentContainer container);

    /**
     * Bubble view has to provide a {@link Text} that will show the index title.
     *
     * @return A {@link Text} that will hold the index title.
     */
    public abstract Text provideBubbleTextView();

    /**
     * To offset the position of the bubble relative to the handle. E.g. in {@link DefaultScrollerViewProvider}
     * the sharp corner of the bubble is aligned with the center of the handle.
     *
     * @return the position of the bubble in relation to the handle (according to the orientation).
     */
    public abstract int getBubbleOffset();

    protected abstract ViewBehavior provideHandleBehavior();

    protected abstract ViewBehavior provideBubbleBehavior();

    protected ViewBehavior getHandleBehavior() {
        if (handleBehavior == null) handleBehavior = provideHandleBehavior();
        return handleBehavior;
    }

    protected ViewBehavior getBubbleBehavior() {
        if (bubbleBehavior == null) bubbleBehavior = provideBubbleBehavior();
        return bubbleBehavior;
    }

    public void onHandleGrabbed() {
        if (getHandleBehavior() != null) getHandleBehavior().onHandleGrabbed();
        if (getBubbleBehavior() != null) getBubbleBehavior().onHandleGrabbed();
    }

    public void onHandleReleased() {
        if (getHandleBehavior() != null) getHandleBehavior().onHandleReleased();
        if (getBubbleBehavior() != null) getBubbleBehavior().onHandleReleased();
    }

    public void onScrollStarted() {
        if (getHandleBehavior() != null) getHandleBehavior().onScrollStarted();
        if (getBubbleBehavior() != null) getBubbleBehavior().onScrollStarted();
    }

    public void onScrollFinished() {
        if (getHandleBehavior() != null) getHandleBehavior().onScrollFinished();
        if (getBubbleBehavior() != null) getBubbleBehavior().onScrollFinished();
    }

}
