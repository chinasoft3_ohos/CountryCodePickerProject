package com.futuremind.recyclerviewfastscroll;

/**
 * ItemSizeProvider
 *
 * @author author
 * @version 1.0
 */
public interface ItemSizeProvider {
    /**
     * getItemHeight
     *
     * @return int
     */
    int getItemHeight();

    /**
     * getItemWidth
     *
     * @return int
     */
    int getItemWidth();
}
