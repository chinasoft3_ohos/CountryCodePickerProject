package com.futuremind.recyclerviewfastscroll;


import ohos.agp.components.Component;
import ohos.agp.components.element.Element;

/**
 * Created by mklimczak on 31/07/15.
 */
public class Utils {
    public static int getViewRawY(Component view) {
        int[] locationOnScreen = ((Component) view.getComponentParent()).getLocationOnScreen();
        return locationOnScreen[1];
    }

    public static int getViewRawX(Component view) {
        int[] locationOnScreen = ((Component) view.getComponentParent()).getLocationOnScreen();
        return locationOnScreen[0];
    }

    public static double getValueInRange(double min, double max, double value) {
        double minimum = Math.max(min, value);
        return Math.min(minimum, max);
    }

    public static void setBackground(Component view, Element drawable) {
        view.setBackground(drawable);
    }


}
