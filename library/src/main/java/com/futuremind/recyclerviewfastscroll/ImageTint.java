/*
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.futuremind.recyclerviewfastscroll;

import ohos.agp.components.Image;
import ohos.agp.components.element.Element;
import ohos.agp.render.BlendMode;
import ohos.agp.utils.Color;

/**
 * ImageTint.
 *
 * @author author
 * @version version
 */
public class ImageTint {
    /**
     * getTintElement.
     *
     * @param element  element
     * @param colorStr element
     * @return Element
     */
    public static Element getTintElement(Element element, String colorStr) {
        int intColor = Color.getIntColor(colorStr);
        return getTintElement(element, intColor);
    }

    /**
     * getTintElement.
     *
     * @param element  element
     * @param intColor element
     * @return Element
     */
    public static Element getTintElement(Element element, int intColor) {

        return getTintElement(element, intColor, BlendMode.SRC_ATOP);
    }

    /**
     * getTintElement.
     *
     * @param element  element
     * @param intColor intColor
     * @param mode     mode
     * @return Element
     */
    public static Element getTintElement(Element element, int intColor, BlendMode mode) {
        int[] colors = new int[]{intColor, intColor};
        int[][] states = new int[2][];
        states[0] = new int[]{0};
        states[1] = new int[]{0};
        element.setStateColorList(states, colors);
        element.setStateColorMode(mode);
        return element;
    }

    /**
     * setImageTint.
     *
     * @param view     view
     * @param element  element
     * @param colorStr colorStr
     */
    public static void setImageTint(Image view, Element element, String colorStr) {
        getTintElement(element, colorStr);
        setImageElement(view, element);
    }

    /**
     * setImageTint.
     *
     * @param view     view
     * @param element  element
     * @param intColor intColor
     */
    public static void setImageTint(Image view, Element element, int intColor) {
        getTintElement(element, intColor);
        setImageElement(view, element);
    }

    /**
     * setImageTint.
     *
     * @param view     view
     * @param element  element
     * @param intColor intColor
     * @param mode     mode
     */
    public static void setImageTint(Image view, Element element, int intColor, BlendMode mode) {
        getTintElement(element, intColor, mode);
        setImageElement(view, element);
    }

    /**
     * setImageElement.
     *
     * @param view    view
     * @param element element
     */
    public static void setImageElement(Image view, Element element) {
        view.setImageElement(element);
    }
}
