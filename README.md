# CountryCodePickerProject

#### 项目介绍
- 项目名称：CountryCodePickerProject
- 所属系列：openharmony的第三方组件适配移植
- 功能：国家城市编码选择
- 项目移植状态：主功能完成
- 调用差异：无
- 开发版本：sdk6，DevEco Studio2.2 beta1
- 基线版本：Release 2.4.7

#### 效果演示
![输入图片说明](./image/CountryCodePickerProject.gif "gif")

#### 安装教程

1.在项目根目录下的build.gradle文件中，

 ```
allprojects {
    repositories {
        maven {
            url 'https://s01.oss.sonatype.org/content/repositories/releases/'
        }
    }
}

 ```

2.在entry模块的build.gradle文件中，

 ```
 dependencies {
    implementation('com.gitee.chinasoft_ohos:country_code_picker:2.4.8')
    ......  
 }

 ```
在sdk6，DevEco Studio2.2 beta1下项目可直接运行
如无法运行，删除项目.gradle,.idea,build,gradle,build.gradle文件，
并依据自己的版本创建新项目，将新项目的对应文件复制到根目录下

#### 使用说明

在xml 中使用

```
    <com.hbb20.CountryCodePicker
      ohos:height="match_content"
      ohos:width="match_content"
      app:xcp_countryPreference="US,IN,NZ"
      app:xcp_defaultNameCode="US"/>
```
在java文件中使用

```java
     CountryCodePicker xcp = (CountryCodePicker) findComponentById(ResourceTable.Id_xcp);
     //调用自定义属性列表中的方法
     xcp.setDefaultCountryUsingPhoneCode();
     xcp.setContentColor();
     xcp.setArrowColor();
     xcp.setFlagSize();
     xcp.setArrowSize();
     xcp.setCustomMasterCountries();
     xcp.setExcludedCountries();
     ...
```
##### 自定义属性 CountryCodePicker

说明：自定义属性名在README替换为xcp,实际使用请使用原属性名

------
| XML Attribute                 | Java method                                                     	| Description                       | Default value  |
|-------------------------	    |-----------------------------------------------------------------	|-----------------------------------|--------------- |
| xcp_defaultPhoneCode          | setDefaultCountryUsingPhoneCode(int defaultCountryCode)           |通过phonecode设置默认国家             | -1             |
| xcp_contentColor              | setContentColor(int contentColor)                                 |设置textarrow颜色                    | BLACK          |
| xcp_arrowColor                | setArrowColor(int arrowColor)                                     |箭头大小颜色                         | -99             |
| xcp_textSize            	    | setFlagSize(int flagSize)                                         |flag大小                            | 18vp           |
| xcp_arrowSize        	        | setArrowSize(int arrowSize)                	                    |箭头大小                             | 0              |          	                                    
| xcp_customMasterCountries     | setCustomMasterCountries(String customMasterCountriesParam)       |自定义的国家列表                      |                |
| xcp_excludedCountries        	| setExcludedCountries(String excludedCountries)                	|设置需要过滤国家                      |                |             	                        
| xcp_flagBorderColor        	| setFlagBorderColor(int borderFlagColor)                	        |边框颜色                            |TRANSPARENT     |                 	                        
| xcp_countryPreference        	| setCountryPreference(String countryPreference)                	|设置国家存储                         |                |
| xcp_defaultNameCode        	| setDefaultCountryUsingNameCode(String defaultCountryNameCode)     |通过CountryNameCode设置默认国家       |                |
| xcp_selectionMemoryTag        |                 	                                                |                                   |                |
| xcp_useFlagEmoji        	    | useFlagEmoji(boolean useFlagEmoji)                 	            |是否使用Emoji                       |false           |
| xcp_useDummyEmojiForPreview   |                 	                                                |                                   |false            |
| xcp_textGravity               | setCurrentTextGravity(TextGravity textGravity)              	    |设置字体的对齐放肆                    |TextGravity.CENTER      |
| xcp_hintExampleNumberType     | setHintExampleNumberType(PhoneNumberType hintExampleNumberType)   |设置PhoneNumber格式                 |PhoneNumberType.MOBILE      |
| xcp_padding                   |                                                                   |设置xcp_padding                     |8px               |
| xcp_showNameCode              | setxcpDialogShowNameCode(boolean xcpDialogShowNameCode)           |设置是否显示 NameCode                |true               |
| xcp_showFlag                  | showFlag(boolean showFlag)                                        |设置是否显示 flag                    |true               |
| xcp_showFullName              | showFullName(boolean showFullName)                                |设置是否显示 FullName                |false               |
| xcp_internationalFormattingOnly | setInternationalFormattingOnly(boolean only)                    |设置是否格式化                       |true               |
| xcp_clickable                 | setxcpClickable(boolean xcpClickable)                             |设置是否可点击                       |true             |
| xcp_showPhoneCode             | setxcpDialogShowPhoneCode(boolean xcpDialogShowPhoneCode)         |设置是否显示PhoneCode                |true               |
| xcp_autoDetectLanguage        |                                                                   |自动编码                            |false               |
| xcp_autoDetectCountry         |                                                                   |自动编码                            |false               |
| xcp_areaCodeDetectedCountry   | setDetectCountryWithAreaCode(boolean detectCountryWithAreaCode)   |detect country from area code      |true               |
| xcp_autoFormatNumber          | setNumberAutoFormattingEnabled(boolean enabled)                   |设置自动编码                         |true              
| xcp_hintExampleNumber         | setHintExampleNumberEnabled(boolean hintExampleNumberEnabled)     |设置是否显示默认提示                  |false               |
| xcp_rememberLastSelection     |                                                                   |                                  |false               |
| xcp_showArrow                 | showArrow(boolean showArrow)                                      |设置是否显示箭头                     |true               |
| xcpDialog_keyboardAutoPopup   |                                                                   |                                  | true              |
| xcpDialog_allowSearch         | setSearchAllowed(boolean searchAllowed)                           |设置Dialog是否显示Search            |true          |
| xcpDialog_showPhoneCode       | setxcpDialogShowPhoneCode(boolean xcpDialogShowPhoneCode)         |设置Dialog是否显示PhoneCode         |true            |
| xcpDialog_showNameCode        | setxcpDialogShowNameCode(boolean xcpDialogShowNameCode)           |设置Dialog是否显示NameCode          |true |
| xcpDialog_showFlag            | setxcpDialogShowFlag(boolean xcpDialogShowFlag)                   |设置Dialog是否显示Flag              |   true            | 
| xcpDialog_showFastScroller    | setShowFastScroller(boolean showFastScroller)                     |设置Dialog是否显示滚动条             |   true            | 
| xcpDialog_backgroundColor     | setDialogBackgroundColor(int dialogBackgroundColor)               |设置Dialog背景颜色                   |     0          | 
| xcpDialog_background          | setDialogBackground(Element dialogBackground)                     |设置Dialog背景                      |               | 
| xcpDialog_cornerRadius        | setDialogCornerRaius(float dialogCornerRadius)                    |设置Dialog圆角                      |      0         | 
| xcpDialog_textColor           | setDialogTextColor(int dialogTextColor)                           |设置Dialog文本颜色                   |     0         | 
| xcpDialog_searchEditTextTint  | setDialogSearchEditTextTintColor(int color)                       |设置Dialog SearchEditTextTint 颜色   |    0           | 
| xcpDialog_showCloseIcon       | showCloseIcon(boolean showCloseIcon)                              |设置Dialog是否显示关闭按钮             |      false         | 
| xcpDialog_showTitle           | setxcpDialogShowTitle(boolean xcpDialogShowTitle)                 |设置Dialog是否显示 Title             |   true            | 
| xcp_defaultLanguage           |                                                                   |设置Dialog默认语言                   |Language.ENGLISH      | 
| xcp_countryAutoDetectionPref  |                                                                   |                                   |     AutoDetectionPref.SIM_NETWORK_LOCALE          | 
| xcpDialog_fastScroller_bubbleColor           | setFastScrollerBubbleColor(int fastScrollerBubbleColor)                        | 设置指示器颜色    | 0        |
| xcpDialog_fastScroller_handleColor           | setFastScrollerHandleColor(int fastScrollerHandleColor)                        | 设置指示器颜色    | 0        |
| xcpDialog_fastScroller_bubbleTextAppearance  | setFastScrollerBubbleTextAppearance(int fastScrollerBubbleTextAppearance)      |                 | 0       |
| xcpDialog_initialScrollToSelection           | enableDialogInitialScrollToSelection(boolean initialScrollToSelection)         |                 | false   |

##### 自定义属性对应枚举
 
 xcp_textGravity
 
```
public enum TextGravity {
        LEFT(-1), CENTER(0), RIGHT(1);
    }
```
 xcp_hintExampleNumberType
```
public enum PhoneNumberType {
        MOBILE,
        FIXED_LINE,
        FIXED_LINE_OR_MOBILE,
        TOLL_FREE,
        PREMIUM_RATE,
        SHARED_COST,
        VOIP,
        PERSONAL_NUMBER,
        PAGER,
        UAN,
        VOICEMAIL,
        UNKNOWN
    }
```
 xcp_defaultLanguage
 
```
public enum Language {
        AFRIKAANS("af"),
        ARABIC("ar"),
        BENGALI("bn"),
        CHINESE_SIMPLIFIED("zh", "CN", "Hans"),
        CHINESE_TRADITIONAL("zh", "TW", "Hant"),
        CZECH("cs"),
        DANISH("da"),
        DUTCH("nl"),
        ENGLISH("en"),
        FARSI("fa"),
        FRENCH("fr"),
        GERMAN("de"),
        GREEK("el"),
        GUJARATI("gu"),
        HEBREW("iw"),
        HINDI("hi"),
        INDONESIA("in"),
        ITALIAN("it"),
        JAPANESE("ja"),
        KAZAKH("kk"),
        KOREAN("ko"),
        MARATHI("mr"),
        POLISH("pl"),
        PORTUGUESE("pt"),
        PUNJABI("pa"),
        RUSSIAN("ru"),
        SLOVAK("sk"),
        SLOVENIAN("si"),
        SPANISH("es"),
        SWEDISH("sv"),
        TAGALOG("tl"),
        TURKISH("tr"),
        UKRAINIAN("uk"),
        URDU("ur"),
        UZBEK("uz"),
        VIETNAMESE("vi");
    }
```
 xcp_countryAutoDetectionPref
 
```
 public enum AutoDetectionPref {
        SIM_ONLY("1"),
        NETWORK_ONLY("2"),
        LOCALE_ONLY("3"),
        SIM_NETWORK("12"),
        NETWORK_SIM("21"),
        SIM_LOCALE("13"),
        LOCALE_SIM("31"),
        NETWORK_LOCALE("23"),
        LOCALE_NETWORK("32"),
        SIM_NETWORK_LOCALE("123"),
        SIM_LOCALE_NETWORK("132"),
        NETWORK_SIM_LOCALE("213"),
        NETWORK_LOCALE_SIM("231"),
        LOCALE_SIM_NETWORK("312"),
        LOCALE_NETWORK_SIM("321");
    }
```

#### 测试信息

CodeCheck代码测试无异常

CloudTest代码测试无异常

病毒安全检测通过

当前版本demo功能与原组件基本无差异


#### 版本迭代

- 2.4.8

- 0.0.1-SNAPSHOT


#### 版权和许可信息

[Apache Version 2.0](./License.txt)

    Copyright (C) 2016 Harsh Bhakta

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.
