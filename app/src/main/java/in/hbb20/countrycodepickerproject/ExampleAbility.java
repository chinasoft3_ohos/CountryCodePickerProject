package in.hbb20.countrycodepickerproject;

import in.hbb20.countrycodepickerproject.slice.ExampleAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;


public class ExampleAbility extends Ability {

    public final static String EXTRA_INIT_TAB = "extraInitTab";

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setMainRoute(ExampleAbilitySlice.class.getName());

    }


}
