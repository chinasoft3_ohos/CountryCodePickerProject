package in.hbb20.countrycodepickerproject.slice;

import com.futuremind.recyclerviewfastscroll.ImageTint;

import in.hbb20.countrycodepickerproject.ExampleAbility;
import in.hbb20.countrycodepickerproject.ResourceTable;

import com.hbb20.utils.Log;

import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.aafwk.content.Operation;
import ohos.agp.components.Component;
import ohos.agp.components.Image;
import ohos.agp.components.element.PixelMapElement;
import ohos.global.resource.Resource;

import java.util.Locale;

import static in.hbb20.countrycodepickerproject.ExampleAbility.EXTRA_INIT_TAB;

public class MainAbilitySlice extends AbilitySlice {
    @Override
    protected void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);
        getWindow().setStatusBarColor(getColor(ResourceTable.Color_colorPrimaryDark));
        assignViews();
        printCountries();
    }

    private void printCountries() {
        String[] list = Locale.getISOCountries();// Fetch list of ISO 3166 country codes
        for (int i = 0; i < list.length; i++) {
            list[i] = new Locale("es", list[i]).getDisplayName();
            Log.d("Country Name", "printCountries: " + list[i]);
        }
    }

    private void assignViews() {

        Resource imageResource = null;
        try {
            imageResource = getResourceManager().getResource(ResourceTable.Media_ic_media_play);
        } catch (Exception e) {
            Log.i("hh", "error" + e.getMessage());
        }
        PixelMapElement pixelMapElement = new PixelMapElement(imageResource);
        pixelMapElement.setFilterPixelMap(true);
        ImageTint.getTintElement(pixelMapElement, "#3F51B5");

        Component textIntro = findComponentById(ResourceTable.Id_textIntro);
        ImageTint.setImageElement((Image) findComponentById(ResourceTable.Id_imageIntro), pixelMapElement);
        setClick(textIntro, 0);

        Component textDefaultCountry = findComponentById(ResourceTable.Id_textDefaultCountry);
        ImageTint.setImageElement((Image) findComponentById(ResourceTable.Id_imageDefaultCountry), pixelMapElement);
        setClick(textDefaultCountry, 1);


        Component textPreference = findComponentById(ResourceTable.Id_textCountryPreference);
        ImageTint.setImageElement((Image) findComponentById(ResourceTable.Id_imageCountryPreference), pixelMapElement);
        setClick(textPreference, 2);

        Component textCustomMaster = findComponentById(ResourceTable.Id_textCustomMaster);
        ImageTint.setImageElement((Image) findComponentById(ResourceTable.Id_imageCustomMaster), pixelMapElement);
        setClick(textCustomMaster, 3);

        Component textSetCountry = findComponentById(ResourceTable.Id_textSetCountry);
        ImageTint.setImageElement((Image) findComponentById(ResourceTable.Id_imageSetCountry), pixelMapElement);
        setClick(textSetCountry, 4);

        Component textGetCountry = findComponentById(ResourceTable.Id_textGetCountry);
        ImageTint.setImageElement((Image) findComponentById(ResourceTable.Id_imageGetCountry), pixelMapElement);
        setClick(textGetCountry, 5);

        Component textFullNumber = findComponentById(ResourceTable.Id_textFullNumber);
        ImageTint.setImageElement((Image) findComponentById(ResourceTable.Id_imageFullNumber), pixelMapElement);
        setClick(textFullNumber, 6);

        Component textCustomColor = findComponentById(ResourceTable.Id_textCustomColor);
        ImageTint.setImageElement((Image) findComponentById(ResourceTable.Id_imageCustomColor), pixelMapElement);
        setClick(textCustomColor, 7);

        Component textCustomSize = findComponentById(ResourceTable.Id_textCustomSize);
        ImageTint.setImageElement((Image) findComponentById(ResourceTable.Id_imageCustomSize), pixelMapElement);
        setClick(textCustomSize, 8);

        Component textCustomFont = findComponentById(ResourceTable.Id_textCustomFont);
        ImageTint.setImageElement((Image) findComponentById(ResourceTable.Id_imageCustomFont), pixelMapElement);
        setClick(textCustomFont, 9);

        Component textCustomLanguage = findComponentById(ResourceTable.Id_textCustomLanguage);
        ImageTint.setImageElement((Image) findComponentById(ResourceTable.Id_imageCustomLanguage), pixelMapElement);
        setClick(textCustomLanguage, 10);

        Component startDemo = findComponentById(ResourceTable.Id_buttonGo);
        startDemo.setClickedListener(component -> {
            startExampleAbility(0);
        });
    }

    private void setClick(Component component, int tabIndex) {
        component.setClickedListener(component1 -> {
            startExampleAbility(tabIndex);
        });

    }

    private void startExampleAbility(int tabIndex) {
        Intent intent = new Intent();
        Operation opt = new Intent
                .OperationBuilder()
                .withBundleName(getBundleName())
                .withAbilityName(ExampleAbility.class.getCanonicalName())
                .build();
        intent.setParam(EXTRA_INIT_TAB, tabIndex);
        intent.setOperation(opt);
        startAbility(intent);
    }
}
