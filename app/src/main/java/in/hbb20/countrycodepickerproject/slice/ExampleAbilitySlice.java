package in.hbb20.countrycodepickerproject.slice;

import in.hbb20.countrycodepickerproject.ResourceTable;
import in.hbb20.countrycodepickerproject.fragment.*;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.PageSlider;
import ohos.agp.window.service.WindowManager;

import java.util.ArrayList;
import java.util.List;

import static in.hbb20.countrycodepickerproject.ExampleAbility.EXTRA_INIT_TAB;

public class ExampleAbilitySlice extends AbilitySlice implements Fragment.ChangeFragmentTool {

    private PageSlider mPageSlider;

    private FragmentPageAdapter mFragmentPageAdapter;

    private int mCurrentIndex = 0;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_example);
        getWindow().setStatusBarColor(getColor(ResourceTable.Color_colorPrimaryDark));
        getWindow().setInputPanelDisplayType(WindowManager.LayoutConfig.INPUT_ADJUST_PAN);
        mCurrentIndex = intent.getIntParam(EXTRA_INIT_TAB, 0);
        initView();
    }


    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
        mFragmentPageAdapter.onForeground(intent);
    }

    @Override
    protected void onBackground() {
        super.onBackground();
        mFragmentPageAdapter.onBackground();
    }

    private void initView() {
        mPageSlider = (PageSlider) findComponentById(ResourceTable.Id_viewPager);
        List<Fragment> list = new ArrayList<>();
        mFragmentPageAdapter = new FragmentPageAdapter(list);
        mPageSlider.setProvider(mFragmentPageAdapter);
        mPageSlider.addPageChangedListener(mFragmentPageAdapter);

        IntroductionFragment introductionFragment = new IntroductionFragment(this);
        introductionFragment.setChangeFragmentTool(this);
        list.add(introductionFragment);

        DefaultCountryFragment defaultCountryFragment = new DefaultCountryFragment(this);
        defaultCountryFragment.setChangeFragmentTool(this);
        list.add(defaultCountryFragment);

        CountryPreferenceFragment countryPreferenceFragment = new CountryPreferenceFragment(this);
        countryPreferenceFragment.setChangeFragmentTool(this);
        list.add(countryPreferenceFragment);

        CustomMasterFragment customMasterFragment = new CustomMasterFragment(this);
        customMasterFragment.setChangeFragmentTool(this);
        list.add(customMasterFragment);

        SetCountryFragment setCountryFragment = new SetCountryFragment(this);
        setCountryFragment.setChangeFragmentTool(this);
        list.add(setCountryFragment);

        GetCountryFragment getCountryFragment = new GetCountryFragment(this);
        getCountryFragment.setChangeFragmentTool(this);
        list.add(getCountryFragment);

        FullNumberFragment fullNumberFragment = new FullNumberFragment(this);
        fullNumberFragment.setChangeFragmentTool(this);
        list.add(fullNumberFragment);

        CustomColorFragment customColorFragment = new CustomColorFragment(this);
        customColorFragment.setChangeFragmentTool(this);
        list.add(customColorFragment);

        CustomSizeFragment customSizeFragment = new CustomSizeFragment(this);
        customSizeFragment.setChangeFragmentTool(this);
        list.add(customSizeFragment);

        CustomFontFragment fontFragment = new CustomFontFragment(this);
        fontFragment.setChangeFragmentTool(this);
        list.add(fontFragment);

        LanguageSupportFragment languageSupportFragment = new LanguageSupportFragment(this);
        languageSupportFragment.setChangeFragmentTool(this);
        list.add(languageSupportFragment);

        mFragmentPageAdapter.notifyDataChanged();
        changeFragment(mCurrentIndex);


    }

    @Override
    public void changeFragment(int index) {
        mPageSlider.setCurrentPage(index);
    }

    @Override
    public int getCurrentPage() {

        return mPageSlider.getCurrentPage();
    }

}
