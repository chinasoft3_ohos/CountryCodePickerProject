package in.hbb20.countrycodepickerproject.fragment;

import com.hbb20.CountryCodePicker;
import com.hbb20.FocusLine;
import in.hbb20.countrycodepickerproject.ResourceTable;
import com.hbb20.utils.Toast;
import ohos.agp.components.*;
import ohos.agp.utils.TextTool;
import ohos.app.Context;

public class DefaultCountryFragment extends Fragment {


    private TextField editTextDefaultPhoneCode;
    private TextField editTextDefaultNameCode;
    private Button buttonSetNewDefaultPhoneCode;
    private Button buttonSetNewDefaultNameCode;
    private Button buttonResetToDefault;
    private CountryCodePicker ccp;
    private Button buttonNext;
    private String lastText = "";

    public DefaultCountryFragment(Context context) {
        super(context);
    }

    @Override
    protected Component onCreateView(LayoutScatter layoutScatter, ComponentContainer container) {
        return layoutScatter.parse(ResourceTable.Layout_fragment_default_country, null, false);

    }

    @Override
    protected void onViewCreate() {
        assignViews();
        editTextWatcher();
        addClickListeners();
    }

    private boolean verification(String s) {
        if (TextTool.isNullOrEmpty(s)) {
            return true;
        }
        String reg = "^\\d+$";
        return s.matches(reg);
    }

    private void assignViews() {
        ScrollView scrollview = (ScrollView) findComponentById(ResourceTable.Id_scrollview);
        editTextDefaultPhoneCode = (TextField) findComponentById(ResourceTable.Id_editText_defaultCode);
        FocusLine.drawFocusLine(editTextDefaultPhoneCode, findComponentById(ResourceTable.Id_component_line2), scrollview);

        ccp = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp);
        buttonSetNewDefaultPhoneCode = (Button) findComponentById(ResourceTable.Id_button_setDefaultCode);
        buttonResetToDefault = (Button) findComponentById(ResourceTable.Id_button_resetToDefault);

        editTextDefaultNameCode = (TextField) findComponentById(ResourceTable.Id_editText_defaultNameCode);
        FocusLine.drawFocusLine(editTextDefaultNameCode, findComponentById(ResourceTable.Id_component_line), scrollview);

        buttonSetNewDefaultNameCode = (Button) findComponentById(ResourceTable.Id_button_setDefaultNameCode);
        buttonNext = (Button) findComponentById(ResourceTable.Id_button_next);
        Text textDef1 = (Text) findComponentById(ResourceTable.Id_text_def1);
        Text textDef2 = (Text) findComponentById(ResourceTable.Id_text_def2);
        String text1 = "<com.hbb20.CountryCodePicker \n ohos:id=\"$+id:ccp\" \n ohos:width=\"match_content\" \n ohos:height=\"match_content\" \n app:ccp_defaultNameCode=\"US\" />";
        textDef1.setText(text1);
        String text2 = "<com.hbb20.CountryCodePicker \n ohos:id=\"$+id:ccp\" \n ohos:width=\"match_content\" \n ohos:height=\"match_content\" \n app:ccp_defaultPhoneCode=\"81\" />";
        textDef2.setText(text2);

    }

    private void addClickListeners() {
        buttonSetNewDefaultPhoneCode.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                try {
                    int code = Integer.parseInt(editTextDefaultPhoneCode.getText().toString());
                    ccp.setDefaultCountryUsingPhoneCode(code);
                    String a = "Now default country is " + ccp.getDefaultCountryName() + " with phone code " + ccp.getDefaultCountryCode();
                    Toast.showShort(mContext, a);
                } catch (Exception e) {
                    Toast.showShort(mContext, "Invalid number format");
                }
            }
        });
        buttonSetNewDefaultNameCode.setClickedListener(component -> {
            String nameCode = "";
            nameCode = editTextDefaultNameCode.getText();
            ccp.setDefaultCountryUsingNameCode(nameCode);
            String a = "Now default country is " + ccp.getDefaultCountryName() + " with phone code " + ccp.getDefaultCountryCode();
            Toast.showShort(mContext, a);
        });

        buttonResetToDefault.setClickedListener(component -> ccp.resetToDefaultCountry());

        buttonNext.setClickedListener(component -> {
            if (mChangeFragment != null) {
                mChangeFragment.changeFragment(mChangeFragment.getCurrentPage() + 1);
            }
        });
    }

    private void editTextWatcher() {
        editTextDefaultPhoneCode.addTextObserver((s, i, i1, i2) -> {
            if (!verification(s)) {
                editTextDefaultPhoneCode.setText(lastText);
                return;
            }
            lastText = s;
            String text = "set " + s + " as Default Country Code";
            buttonSetNewDefaultPhoneCode.setText(text);
        });
        editTextDefaultNameCode.addTextObserver((s, i, i1, i2) -> {
            String text = "set " + s + " as Default Country Name Code";
            buttonSetNewDefaultNameCode.setText(text);
        });
    }
}
