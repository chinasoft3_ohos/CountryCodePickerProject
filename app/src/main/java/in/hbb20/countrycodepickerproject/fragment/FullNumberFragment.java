package in.hbb20.countrycodepickerproject.fragment;

import com.hbb20.CountryCodePicker;
import com.hbb20.FocusLine;
import in.hbb20.countrycodepickerproject.ResourceTable;
import ohos.agp.components.*;
import ohos.agp.components.element.VectorElement;
import ohos.agp.utils.TextTool;
import ohos.app.Context;

public class FullNumberFragment extends Fragment {

    private TextField editTextLoadFullNumber;
    private TextField editTextLoadCarrierNumber;
    private TextField editTextGetCarrierNumber;
    private Text editTextGetFullNumber;
    private CountryCodePicker ccpLoadNumber;
    private CountryCodePicker ccpGetNumber;
    private Text tvValidity;
    private Image imgValidity;
    private Button buttonLoadNumber;
    private Button buttonGetNumber;
    private Button buttonGetNumberWithPlus;
    private Button buttonFormattedFullNumber;
    private Button buttonNext;
    private String lastText;


    public FullNumberFragment(Context context) {
        super(context);
    }

    @Override
    protected Component onCreateView(LayoutScatter layoutScatter, ComponentContainer container) {
        return layoutScatter.parse(ResourceTable.Layout_fragment_full_number, null, false);
    }

    @Override
    protected void onViewCreate() {
        assignView();
        registerCarrierEditText();
        setClickListener();
        addTextWatcher();
    }

    private void assignView() {
        //load number
        ScrollView  scrollview = (ScrollView) findComponentById(ResourceTable.Id_scrollview);
        editTextLoadFullNumber = (TextField) findComponentById(ResourceTable.Id_editText_loadFullNumber);
        FocusLine.drawFocusLine(editTextLoadFullNumber, findComponentById(ResourceTable.Id_component_line),scrollview);

        editTextLoadCarrierNumber = (TextField) findComponentById(ResourceTable.Id_editText_loadCarrierNumber);
        FocusLine.drawFocusLine(editTextLoadCarrierNumber, findComponentById(ResourceTable.Id_component_line2),scrollview);

        ccpLoadNumber = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp_loadFullNumber);
        buttonLoadNumber = (Button) findComponentById(ResourceTable.Id_button_loadFullNumber);

        editTextGetCarrierNumber = (TextField) findComponentById(ResourceTable.Id_editText_getCarrierNumber);
        FocusLine.drawFocusLine(editTextGetCarrierNumber, findComponentById(ResourceTable.Id_component_line3),scrollview);

        editTextGetFullNumber = (Text) findComponentById(ResourceTable.Id_textView_getFullNumber);
        buttonGetNumber = (Button) findComponentById(ResourceTable.Id_button_getFullNumber);
        buttonGetNumberWithPlus = (Button) findComponentById(ResourceTable.Id_button_getFullNumberWithPlus);
        ccpGetNumber = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp_getFullNumber);
        buttonFormattedFullNumber = (Button) findComponentById(ResourceTable.Id_button_getFormattedFullNumberWithPlus);
        tvValidity = (Text) findComponentById(ResourceTable.Id_tv_validity);
        imgValidity = (Image) findComponentById(ResourceTable.Id_img_validity);

        buttonNext = (Button) findComponentById(ResourceTable.Id_button_next);
    }

    private void registerCarrierEditText() {
        ccpLoadNumber.registerCarrierNumberEditText(editTextLoadCarrierNumber);
        ccpGetNumber.registerCarrierNumberEditText(editTextGetCarrierNumber);
        ccpGetNumber.setPhoneNumberValidityChangeListener(isValidNumber -> {
            int drawableId = 0;
            if (isValidNumber) {
                drawableId = ResourceTable.Graphic_ic_assignment_turned_in_black_24dp;
                tvValidity.setText("Valid Number");
            } else {
                drawableId = ResourceTable.Graphic_ic_assignment_late_black_24dp;
                tvValidity.setText("Invalid Number");
            }
            VectorElement vectorElement = new VectorElement(mContext, drawableId);
            imgValidity.setImageElement(vectorElement);
        });


        ccpLoadNumber.registerCarrierNumberEditText(editTextLoadCarrierNumber);
    }

    private void setClickListener() {

        buttonLoadNumber.setClickedListener(component -> ccpLoadNumber.setFullNumber(editTextLoadFullNumber.getText()));

        buttonFormattedFullNumber.setClickedListener(component -> editTextGetFullNumber.setText(ccpGetNumber.getFormattedFullNumber()));

        buttonGetNumber.setClickedListener(component -> editTextGetFullNumber.setText(ccpGetNumber.getFullNumber()));

        buttonGetNumberWithPlus.setClickedListener(component -> editTextGetFullNumber.setText(ccpGetNumber.getFullNumberWithPlus()));

        buttonNext.setClickedListener(component -> {
            if (mChangeFragment != null) {
                mChangeFragment.changeFragment(mChangeFragment.getCurrentPage() + 1);
            }
        });
    }

    private boolean verification(String s) {
        if (TextTool.isNullOrEmpty(s)) {
            return true;
        }
        String reg = "^\\d+$";
        return s.matches(reg);
    }
    private void addTextWatcher() {

        editTextLoadFullNumber.addTextObserver((s, i, i1, i2) -> {
            if (!verification(s)) {
                editTextLoadFullNumber.setText(lastText);
                return;
            }
            lastText = s;
            String text = "Load " + s + " to CCP.";
            buttonLoadNumber.setText(text);
        });


    }
}
