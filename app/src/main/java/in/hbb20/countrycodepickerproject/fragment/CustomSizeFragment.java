package in.hbb20.countrycodepickerproject.fragment;

import in.hbb20.countrycodepickerproject.ResourceTable;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.app.Context;

public class CustomSizeFragment extends Fragment {

    public CustomSizeFragment(Context context) {
        super(context);
    }

    @Override
    protected Component onCreateView(LayoutScatter layoutScatter, ComponentContainer container) {
        return layoutScatter.parse(ResourceTable.Layout_fragment_custom_size, null, false);
    }

    @Override
    protected void onViewCreate() {
        findComponentById(ResourceTable.Id_button_next).setClickedListener(component -> {
            if (mChangeFragment != null) {
                mChangeFragment.changeFragment(mChangeFragment.getCurrentPage() + 1);
            }
        });
    }
}
