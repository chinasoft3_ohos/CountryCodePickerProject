package in.hbb20.countrycodepickerproject.fragment;

import com.hbb20.CountryCodePicker;
import com.hbb20.FocusLine;
import in.hbb20.countrycodepickerproject.CCPCustomTalkBackProvider;
import in.hbb20.countrycodepickerproject.ResourceTable;
import ohos.agp.components.*;
import ohos.app.Context;

public class IntroductionFragment extends Fragment {


    public IntroductionFragment(Context context) {
        super(context);
    }

    @Override
    protected Component onCreateView(LayoutScatter layoutScatter, ComponentContainer container) {
        return layoutScatter.parse(ResourceTable.Layout_fragment_introduction, null, false);
    }

    @Override
    protected void onViewCreate() {
        CountryCodePicker  countryCodePicker = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp);
        TextField  etPhone = (TextField) findComponentById(ResourceTable.Id_et_phone);
        ScrollView scrollview = (ScrollView) findComponentById(ResourceTable.Id_scrollview);
        findComponentById(ResourceTable.Id_button_letsGo).setClickedListener(component -> {
            if (mChangeFragment != null) {
                mChangeFragment.changeFragment(mChangeFragment.getCurrentPage() + 1);
            }
        });
        countryCodePicker.registerCarrierNumberEditText(etPhone);
        countryCodePicker.setTalkBackTextProvider(new CCPCustomTalkBackProvider());
        TextField textField = (TextField) findComponentById(ResourceTable.Id_custom_textfield);
        Component line = findComponentById(ResourceTable.Id_component_line);
        FocusLine.drawFocusLine(textField, line, scrollview);
        Component line2 = findComponentById(ResourceTable.Id_component_line2);
        FocusLine.drawFocusLine(etPhone, line2, scrollview);


    }
}
