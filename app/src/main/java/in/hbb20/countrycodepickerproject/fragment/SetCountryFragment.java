package in.hbb20.countrycodepickerproject.fragment;

import com.hbb20.CountryCodePicker;
import com.hbb20.FocusLine;
import com.hbb20.utils.Log;
import in.hbb20.countrycodepickerproject.ResourceTable;
import com.hbb20.utils.Toast;
import ohos.agp.components.*;
import ohos.agp.utils.TextTool;
import ohos.app.Context;

public class SetCountryFragment extends Fragment {

    private TextField editTextCode;
    private TextField editTextNameCode;
    private Button buttonSetCode;
    private Button buttonSetNameCode;
    private CountryCodePicker ccp;
    private Button buttonNext;
    private String lastText = "";

    public SetCountryFragment(Context context) {
        super(context);
    }

    @Override
    protected Component onCreateView(LayoutScatter layoutScatter, ComponentContainer container) {
        return layoutScatter.parse(ResourceTable.Layout_fragment_set_country, null, false);
    }

    @Override
    protected void onViewCreate() {
        assignViews();
        editTextWatcher();
        addClickListeners();
    }

    private void assignViews() {
        ScrollView scrollview = (ScrollView) findComponentById(ResourceTable.Id_scrollview);
        editTextNameCode = (TextField) findComponentById(ResourceTable.Id_editText_countryNameCode);
        FocusLine.drawFocusLine(editTextNameCode, findComponentById(ResourceTable.Id_component_line2), scrollview);
        editTextCode = (TextField) findComponentById(ResourceTable.Id_editText_countryCode);
        FocusLine.drawFocusLine(editTextCode, findComponentById(ResourceTable.Id_component_line), scrollview);
        ccp = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp);
        buttonSetCode = (Button) findComponentById(ResourceTable.Id_button_setCountry);
        buttonSetNameCode = (Button) findComponentById(ResourceTable.Id_button_setCountryNameCode);
        buttonNext = (Button) findComponentById(ResourceTable.Id_button_next);
    }

    private void addClickListeners() {

        buttonSetCode.setClickedListener(component -> {
            try {
                int code = Integer.parseInt(editTextCode.getText());
                ccp.setCountryForPhoneCode(code);
            } catch (Exception e) {
                Toast.showShort(mContext, "Invalid number format");
            }
        });
        buttonSetNameCode.setClickedListener(component -> {
            try {
                String code = editTextNameCode.getText();
                ccp.setCountryForNameCode(code);
            } catch (Exception e) {
                Log.i("hh", "error" + e.getMessage());
            }
        });
        buttonNext.setClickedListener(component -> {
            if (mChangeFragment != null) {
                mChangeFragment.changeFragment(mChangeFragment.getCurrentPage() + 1);
            }
        });


    }

    private boolean verification(String s) {
        if (TextTool.isNullOrEmpty(s)) {
            return true;
        }
        String reg = "^\\d+$";
        return s.matches(reg);
    }

    private void editTextWatcher() {

        editTextCode.addTextObserver((s, i, i1, i2) -> {

            if (!verification(s)) {
                editTextCode.setText(lastText);
                return;
            }
            lastText = s;

            String text = "Set country with code " + s;
            buttonSetCode.setText(text);

        });

        editTextNameCode.addTextObserver((s, i, i1, i2) -> {
            String text = "Set country with name code " + s;
            buttonSetNameCode.setText(text);
        });

        ccp.setOnCountryChangeListener(() -> {
            String text = "This is from OnCountryChangeListener. \n Country updated to " + ccp.getSelectedCountryName() + "(" + ccp.getSelectedCountryCodeWithPlus() + ")";
            Toast.showShort(mContext, text);
        });

    }
}
