package in.hbb20.countrycodepickerproject.fragment;

import com.hbb20.CountryCodePicker;
import com.hbb20.utils.Log;
import in.hbb20.countrycodepickerproject.ResourceTable;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.ComponentContainer;
import ohos.agp.components.LayoutScatter;
import ohos.agp.text.Font;
import ohos.app.Context;
import ohos.app.Environment;
import ohos.global.resource.RawFileEntry;
import ohos.global.resource.Resource;
import ohos.global.resource.ResourceManager;

import java.io.*;

public class CustomFontFragment extends Fragment {

    private Button buttonNext;
    private CountryCodePicker ccp6;
    private CountryCodePicker ccp5;
    private CountryCodePicker ccp4;
    private CountryCodePicker ccp3;
    private CountryCodePicker ccp2;

    public CustomFontFragment(Context context) {
        super(context);
    }

    @Override
    protected Component onCreateView(LayoutScatter layoutScatter, ComponentContainer container) {
        return layoutScatter.parse(ResourceTable.Layout_fragment_custom_font, null, false);
    }

    @Override
    protected void onViewCreate() {
        assignViews();
        applyCustomFonts();
        setClickListener();
    }

    private void assignViews() {
        ccp2 = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp2);
        ccp3 = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp3);
        ccp4 = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp4);
        ccp5 = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp5);
        ccp6 = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp6);
        buttonNext = (Button) findComponentById(ResourceTable.Id_button_next);
    }

    private void applyCustomFonts() {
        setTTFfont(ccp2, "bookos.ttf");
        setTTFfont(ccp3, "hack.ttf");
        setTTFfont(ccp4, "playfair.ttf");
        setTTFfont(ccp5, "raleway.ttf");
        setTTFfont(ccp6, "titillium.ttf");
    }

    private void setTTFfont(CountryCodePicker ccp, String fontFileName) {

        String filePath = String.format("resources/rawfile/fonts/%s", fontFileName);
        ResourceManager resManager = mContext.getResourceManager();
        RawFileEntry rawFileEntry = resManager.getRawFileEntry(filePath);
        Resource resource = null;
        try {
            resource = rawFileEntry.openRawFile();
        } catch (IOException e) {
            Log.i("hh", "error" + e.getMessage());
        }
        if (resource == null) {
            return;
        }
        StringBuffer fileName = new StringBuffer(fontFileName);
        File file = new File(mContext.getExternalFilesDir(Environment.DIRECTORY_PICTURES), fileName.toString());
        OutputStream outputStream = null;
        try {
            outputStream = new FileOutputStream(file);
            int index;
            byte[] bytes = new byte[1024];
            while ((index = resource.read(bytes)) != -1) {
                outputStream.write(bytes, 0, index);
                outputStream.flush();
            }
        } catch (FileNotFoundException e) {
            Log.i("hh", "error" + e.getMessage());
        } catch (IOException e) {
            Log.i("hh", "error" + e.getMessage());
        } finally {
            try {
                resource.close();
            } catch (IOException e) {
                Log.i("hh", "error" + e.getMessage());
            }
            try {
                if (outputStream != null) {
                    outputStream.close();
                }
            } catch (IOException e) {
                Log.i("hh", "error" + e.getMessage());
            }
        }
        Font.Builder builder = new Font.Builder(file);
        Font font = builder.build();
        ccp.setTypeFace(font);
    }


    private void setClickListener() {
        buttonNext.setClickedListener(component -> {
            if (mChangeFragment != null) {
                mChangeFragment.changeFragment(mChangeFragment.getCurrentPage() + 1);
            }
        });
    }
}
