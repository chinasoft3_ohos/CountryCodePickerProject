package in.hbb20.countrycodepickerproject.fragment;

import com.hbb20.CountryCodePicker;
import in.hbb20.countrycodepickerproject.ResourceTable;

import ohos.agp.colors.RgbColor;
import ohos.agp.components.*;
import ohos.agp.components.element.ShapeElement;
import ohos.agp.utils.Color;
import ohos.app.Context;

public class CustomColorFragment extends Fragment {

    private Button buttonNext;
    private Text textViewTitle;
    private TextField editTextPhone;
    private CountryCodePicker ccp;
    private DependentLayout relativeColor1;
    private DependentLayout relativeColor2;
    private DependentLayout relativeColor3;
    private Component line;
    private int selectedColor = -1;

    public CustomColorFragment(Context context) {
        super(context);
    }

    @Override
    protected Component onCreateView(LayoutScatter layoutScatter, ComponentContainer container) {
        return layoutScatter.parse(ResourceTable.Layout_fragment_custom_color, null, false);
    }

    @Override
    protected void onViewCreate() {
        assignViews();
        setClickListener();
    }

    private void assignViews() {
        textViewTitle = (Text) findComponentById(ResourceTable.Id_textView_title);
        editTextPhone = (TextField) findComponentById(ResourceTable.Id_editText_phone);
        ccp = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp);
        relativeColor1 = (DependentLayout) findComponentById(ResourceTable.Id_relative_color1);
        relativeColor2 = (DependentLayout) findComponentById(ResourceTable.Id_relative_color2);
        relativeColor3 = (DependentLayout) findComponentById(ResourceTable.Id_relative_color3);
        buttonNext = (Button) findComponentById(ResourceTable.Id_button_next);
        line = findComponentById(ResourceTable.Id_component_line2);
    }

    private void setClickListener() {
        relativeColor1.setClickedListener(component -> {
            int color = mContext.getColor(ResourceTable.Color_color1);
            setColor(1, color);
            setLineBg(color);

        });
        relativeColor2.setClickedListener(component -> {
            int color = mContext.getColor(ResourceTable.Color_color2);
            setColor(2, color);
            setLineBg(color);
        });
        relativeColor3.setClickedListener(component -> {
            int color = mContext.getColor(ResourceTable.Color_color3);
            setColor(3, color);
            setLineBg(color);
        });


        buttonNext.setClickedListener(component -> {
            if (mChangeFragment != null) {
                mChangeFragment.changeFragment(mChangeFragment.getCurrentPage() + 1);
            }
        });


        ccp.setClickedListener(component -> {
            ccp.launchCountrySelectionDialog();
            ccp.setContentColor(selectedColor);
        });
    }

    private void setColor(int selection, int color) {
        ccp.setContentColor(color);
        Color colors = new Color(color);
        textViewTitle.setTextColor(colors);

        editTextPhone.setTextColor(colors);
        editTextPhone.setHintColor(colors);

        resetBG();

        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#ff222124")));
        if (selection == 1) {
            relativeColor1.setBackground(shapeElement);
        } else if (2 == selection) {
            relativeColor2.setBackground(shapeElement);
        } else if (3 == selection) {
            relativeColor3.setBackground(shapeElement);
        }
    }

    private void resetBG() {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(Color.getIntColor("#ffe5e5e5")));
        relativeColor1.setBackground(shapeElement);
        relativeColor2.setBackground(shapeElement);
        relativeColor3.setBackground(shapeElement);
    }

    private void setLineBg(int color) {
        ShapeElement shapeElement = new ShapeElement();
        shapeElement.setRgbColor(RgbColor.fromArgbInt(color));
        line.setBackground(shapeElement);

    }
}
