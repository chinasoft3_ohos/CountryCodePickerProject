package in.hbb20.countrycodepickerproject.fragment;

import com.hbb20.CountryCodePicker;
import in.hbb20.countrycodepickerproject.ResourceTable;
import com.hbb20.utils.Toast;
import ohos.agp.components.*;
import ohos.app.Context;

import java.util.ArrayList;
import java.util.List;

import static com.hbb20.CountryCodePicker.Language.*;

public class LanguageSupportFragment extends Fragment {

    private DirectionalLayout radioEnglish;
    private DirectionalLayout radioJapanese;
    private DirectionalLayout radioSpanish;
    private CountryCodePicker ccp;
    private Button buttonNext;
    private int selectIndex = 0;
    private List<Image> list = new ArrayList<>();

    public LanguageSupportFragment(Context context) {
        super(context);
    }

    @Override
    protected Component onCreateView(LayoutScatter layoutScatter, ComponentContainer container) {
        return layoutScatter.parse(ResourceTable.Layout_fragment_language_support, null, false);
    }

    @Override
    protected void onViewCreate() {
        assignViews();
        setClickListener();
    }

    private void assignViews() {

        ccp = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp);
        radioEnglish = (DirectionalLayout) findComponentById(ResourceTable.Id_radioEnglish);
        radioJapanese = (DirectionalLayout) findComponentById(ResourceTable.Id_radioJapanese);
        radioSpanish = (DirectionalLayout) findComponentById(ResourceTable.Id_radioSpanish);
        buttonNext = (Button) findComponentById(ResourceTable.Id_button_next);
        Image image_radioEnglish = (Image) findComponentById(ResourceTable.Id_image_radioEnglish);
        Image image_radioJapanese = (Image) findComponentById(ResourceTable.Id_image_radioJapanese);
        Image image_radioSpanish = (Image) findComponentById(ResourceTable.Id_image_radioSpanish);
        list.clear();
        list.add(image_radioEnglish);
        list.add(image_radioJapanese);
        list.add(image_radioSpanish);
        setDefView();
    }


    private void setClickListener() {

        radioEnglish.setClickedListener(component -> {
            this.selectIndex = 0;
            setDefView();
            ccp.changeDefaultLanguage(ENGLISH);
            Toast.showShort(mContext, "Language is updated to ENGLISH");
        });
        radioJapanese.setClickedListener(component -> {
            this.selectIndex = 1;
            setDefView();
            ccp.changeDefaultLanguage(JAPANESE);
            Toast.showShort(mContext, "Language is updated to JAPANESE");
        });
        radioSpanish.setClickedListener(component -> {
            this.selectIndex = 2;
            setDefView();
            ccp.changeDefaultLanguage(SPANISH);
            Toast.showShort(mContext, "Language is updated to SPANISH");
        });

        buttonNext.setClickedListener(component -> {
            if (mChangeFragment != null) {
                mChangeFragment.changeFragment(mChangeFragment.getCurrentPage() + 1);
            }
        });
    }

    private void setDefView() {
        for (int i = 0; i < list.size(); i++) {
            Image image = list.get(i);
            if (i == selectIndex) {
                image.setPixelMap(ResourceTable.Media_icon_rb_s);
            } else {
                image.setPixelMap(ResourceTable.Media_icon_rb_df);
            }
        }
    }
}
