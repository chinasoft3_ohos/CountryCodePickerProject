package in.hbb20.countrycodepickerproject.fragment;

import com.hbb20.CountryCodePicker;

import in.hbb20.countrycodepickerproject.ResourceTable;

import ohos.agp.components.*;
import ohos.app.Context;

public class GetCountryFragment extends Fragment {

    private Text textViewCountryName;
    private Text textViewCountryCode;
    private Text textViewCountryNameCode;
    private Button buttonReadCountry;
    private CountryCodePicker ccp;
    private Button buttonNext;


    public GetCountryFragment(Context context) {
        super(context);
    }

    @Override
    protected Component onCreateView(LayoutScatter layoutScatter, ComponentContainer container) {
        return layoutScatter.parse(ResourceTable.Layout_fragment_get_country, null, false);
    }

    @Override
    protected void onViewCreate() {
        assignViews();
        setClickListener();
    }

    private void setClickListener() {
        buttonReadCountry.setClickedListener(component -> {
            textViewCountryName.setText(ccp.getSelectedCountryName());
            textViewCountryCode.setText(ccp.getSelectedCountryCode());
            textViewCountryNameCode.setText(ccp.getSelectedCountryNameCode());
        });

        buttonNext.setClickedListener(component -> {
            if (mChangeFragment != null) {
                mChangeFragment.changeFragment(mChangeFragment.getCurrentPage() + 1);
            }
        });

    }

    private void assignViews() {
        ccp = (CountryCodePicker) findComponentById(ResourceTable.Id_ccp);
        textViewCountryCode = (Text) findComponentById(ResourceTable.Id_textView_countryCode);
        textViewCountryName = (Text) findComponentById(ResourceTable.Id_textView_countryName);
        textViewCountryNameCode = (Text) findComponentById(ResourceTable.Id_textView_countryNameCode);
        buttonReadCountry = (Button) findComponentById(ResourceTable.Id_button_readCountry);
        buttonNext = (Button) findComponentById(ResourceTable.Id_button_next);
    }
}
