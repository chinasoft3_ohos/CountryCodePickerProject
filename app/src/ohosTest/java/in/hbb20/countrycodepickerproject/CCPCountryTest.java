package in.hbb20.countrycodepickerproject;

import com.hbb20.CCPCountry;

import org.junit.Test;

import java.util.List;

/**
 * 单元测试
 *
 * @时间： 2021/06/10
 * @描述：
 */
public class CCPCountryTest {

    /**
     * testToString.
     */
    @Test
    public void testToString() {
        CCPCountry ccpCountry = new CCPCountry();
        CCPCountry ccpCountryPar = new CCPCountry("cn", "86", "china", 0);
    }

    /**
     * getLibraryMasterCountriesEnglish.
     */
    @Test
    public void getLibraryMasterCountriesEnglish() {
        List<CCPCountry> libraryMasterCountriesEnglish = CCPCountry.getLibraryMasterCountriesEnglish();
    }
}